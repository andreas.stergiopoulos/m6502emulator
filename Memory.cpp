#include "Memory.h"

Memory::Memory(uint32_t size)
{
    if (size < 4096)
    {
        throw "Memory must have a size greater than 4KB!";
    }
    else
    {
        Mem_Size = size;
        Data = new BYTE[Mem_Size];
        this->InitializeMemory();
    }
}
void Memory::InitializeMemory()
{
    FillMemory(0);
}
void Memory::FillMemory(uint32_t value)
{
    for (int i = 0; i < Mem_Size; ++i)
    {
        Data[i] = value;
    }
}
BYTE Memory::ReadByteFromAddress(ADDRESS address)
{
    if (address < Mem_Size)
    {
        return Data[address];
    }
    else
    {
        throw "Memory acces out of bounds!";
        return 0;
    }
}


ADDRESS Memory::ReadAddressFromAddress(ADDRESS address)
{
    if (address < Mem_Size - 1)
    {
        return ((Data[address+1] << 8) + Data[address]);
    }
    else
    {
        throw "Memory acces out of bounds!";
        return 0;
    }
}

void Memory::WriteByteToAddress(ADDRESS address, BYTE byte)
{
    if (address < Mem_Size)
    {
        Data[address] = byte;
    }
    else
    {
        throw "Memory acces out of bounds!";
    }
}
void Memory::WriteAddressToAddress(ADDRESS address, ADDRESS word)
{
    if (address < Mem_Size)
    {
        Data[address] = (BYTE)(word & 0xFF);
        Data[address + 1] = (BYTE)(word >> 8);
    }
    else
    {
        throw "Memory acces out of bounds!";
    }
}

void Memory::DebugWrite(ADDRESS address, BYTE value)
{
    if (address > Mem_Size)
        throw "Memory array index out of bounds!";
    else
        Data[address] = value;
}

void Memory::DebugWrite(ADDRESS address, ADDRESS value)
{
    if (address > Mem_Size)
        throw "Memory array index out of bounds!";
    else
    {
        Data[address] = (BYTE)(value & 0xFF);
        Data[address + 1] = (BYTE)(value >> 8);
    }
}

BYTE Memory::DebugRead(ADDRESS address)
{
    return Data[address];
}
