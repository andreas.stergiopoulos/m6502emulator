#ifndef MEMORY_H
#define MEMORY_H

#include <stdlib.h>

#ifndef DEFS_H
    #include "defs.h"
#endif

class Memory
{
public:
    Memory(uint32_t size);

    void InitializeMemory();

    void FillMemory(uint32_t value);

    BYTE ReadByteFromAddress(ADDRESS address);

    ADDRESS ReadAddressFromAddress(ADDRESS address);

    void WriteByteToAddress(ADDRESS address, BYTE byte);

    void WriteAddressToAddress(ADDRESS address, ADDRESS value);

    void DebugWrite(ADDRESS address, BYTE value);

    void DebugWrite(ADDRESS address, ADDRESS value);

    BYTE DebugRead(ADDRESS address);

private:
    uint32_t Mem_Size;
    BYTE *Data = NULL;
};

#endif