#include <iostream>
#include <stdlib.h>
#include <math.h>

#ifndef CPU_H
    #include "CPU.h"
#endif

int main(int, char **)
{
    Clock clock;
    Memory mem(pow(2, 16));
    CPU Motorola6502;
    mem.InitializeMemory();
    Motorola6502.Reset(mem);
    /*Inline assembly program start*/

    /*Inline assembly program end*/
    Motorola6502.Execute(clock, mem);
    std::cout << "The emulator finished executing!\n";
    return 0;
}
