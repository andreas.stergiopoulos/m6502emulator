#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(PHP_Tests, Test_PHP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------PUSH PROGRAM STATUS TO STACK-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)PHP);
    M6502.RegisterFile.PS.C = 1;
    M6502.RegisterFile.PS.Z = 1;

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(cycles, 3);
    EXPECT_EQ(mem.DebugRead(M6502.RegisterFile.SP+0x100+1), M6502.RegisterFile.PS.ToByte());
}
