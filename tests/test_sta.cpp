#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(STA_Tests, Test_STA_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------STORE A TO ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)STA_ZP);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0xD0), 0x42);
    EXPECT_EQ(cycles, 3);
}

TEST(STA_Tests, Test_STA_ZP_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------STORE A TO ZERO PAGE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.X = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)STA_ZP_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x11), 0x42);
    EXPECT_EQ(cycles, 4);
}

TEST(STA_Tests, Test_STA_ABS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD A FROM ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)STA_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4242), 0x42);
    EXPECT_EQ(cycles, 4);
}

TEST(STA_Tests, Test_STA_ABS_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------STORE A TO ABSOLUTE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.X = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)STA_ABS_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4262), 0x42);
    EXPECT_EQ(cycles, 5);
}

TEST(STA_Tests, Test_STA_ABS_Y)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------STORE A TO ABSOLUTE + Y-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.Y = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)STA_ABS_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4262), 0x42);
    EXPECT_EQ(cycles, 5);

    /**---------------STORE A TO INDEXED INDIRECT-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.X = 0x4;
    mem.DebugWrite(0xFCE2, (BYTE)STA_IND_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x20);
    mem.DebugWrite(0x24, (BYTE)0x74);
    mem.DebugWrite(0x24 + 1, (BYTE)0x20);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x2074), 0x42);
    EXPECT_EQ(cycles, 6);

    /**---------------STORE A TO INDIRECT INDEXED-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.Y = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)STA_IND_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x86);
    mem.DebugWrite(0x86, (BYTE)0x74);
    mem.DebugWrite(0x86 + 1, (BYTE)0x20);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x2094), 0x42);
    EXPECT_EQ(cycles, 6);
}

TEST(STA_Tests, Test_STA_ABS_IXD_ICT)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------STORE A TO INDEXED INDIRECT-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.X = 0x4;
    mem.DebugWrite(0xFCE2, (BYTE)STA_IND_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x20);
    mem.DebugWrite(0x24, (BYTE)0x74);
    mem.DebugWrite(0x24 + 1, (BYTE)0x20);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x2074), 0x42);
    EXPECT_EQ(cycles, 6);
}

TEST(STA_Tests, Test_STA_ABS_ICT_IXD)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------STORE A TO INDIRECT INDEXED-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.Y = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)STA_IND_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x86);
    mem.DebugWrite(0x86, (BYTE)0x74);
    mem.DebugWrite(0x86 + 1, (BYTE)0x20);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x2094), 0x42);
    EXPECT_EQ(cycles, 6);
}
