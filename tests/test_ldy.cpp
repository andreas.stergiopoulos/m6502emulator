#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(LDY_Tests, Test_LDY_IM)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------LOAD Y WITH IMMEDIATE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)LDY_IM);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.Y, 0xD0);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 2);
}

TEST(LDY_Tests, Test_LDY_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD Y FROM ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)LDY_ZP);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xD0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.Y, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 3);
}

TEST(LDY_Tests, Test_LDY_ZP_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD Y FROM ZERO PAGE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)LDY_ZP_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xE0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.Y, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 4);
}

TEST(LDY_Tests, Test_LDY_ABS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD Y FROM ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)LDY_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x42D0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.Y, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 4);
}

TEST(LDY_Tests, Test_LDY_ABS_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD Y FROM ABSOLUTE + X-----------------*/
    /**-------SUBCASE WITHOUT PAGE CROSS-------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)LDY_ABS_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x42E0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.Y, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 4);

    /**-------SUBCASE WITH PAGE CROSS-------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)LDY_ABS_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x43CF, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.Y, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 5);
}
