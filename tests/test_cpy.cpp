#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

/**
 * Due to the fact that all comparison results have been tested in the according
 * testing suite, this suite only checks for the equality case. The goal of this 
 * test suite is to determine if the instruction operates correctly regarding the
 * timing and the address space.
*/

TEST(CPY_Tests, Test_CPY_IM)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP Y WITH IMMEDIATE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)CPY_IM);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x10);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 2);
}

TEST(CPY_Tests, Test_CPY_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP Y FROM ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)CPY_ZP);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);
    mem.DebugWrite(0x1, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 3);
}

TEST(CPY_Tests, Test_CPY_ABS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP Y FROM ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)CPY_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4242, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 4);
}
