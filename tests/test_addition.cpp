#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(Binary_Addition_Tests, Test_Unsigned_No_Carry)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    /**---------------UNSIGNED NUMBER ADDITION WITHOUT CARRY-----------------*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 0, 0, 0, 0 };
    expected.number = 62;

    /** When: */
    actual = M6502.Add(24, 38);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);

    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 1, 0, 0, 0, 0, 0, 0 };
    expected.number = 61;

    /** When: */
    actual = M6502.Add(255, 62);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(Binary_Addition_Tests, Test_Unsigned_Carry)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;

    /**---------------UNSIGNED NUMBER ADDITION WITH CARRY-----------------*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 1, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 0, 0, 0, 0 };
    expected.number = 63;

    /** When: */
    actual = M6502.Add(24, 38);
    actual.flags.C = 0; // Clear the Carry flag propagated from the start condition.

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(Binary_Addition_Tests, Test_Signed_Carry)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;

    /**---------------SIGNED NUMBER ADDITION WITH CARRY-----------------*/
    /**FOR SAME-SIGN, POSITIVE INTEGERS, SAME AS UNSIGNED ADDITION WITH CARRY*/
    /**FOR SAME-SIGN, NEGATIVE INTEGERS*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 1, 0, 0, 0, 0, 0, 1 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 1, 0, 0, 0, 0, 0, 1 };
    expected.number = -63;

    /** When: */
    actual = M6502.Add(-60, -4);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);

    /**FOR OPPOSITE-SIGN, NEGATIVE INTEGERS*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 1, 0, 0, 0, 0, 0, 1 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 1, 0, 0, 0, 0, 0, 0 };
    expected.number = 38;

    /** When: */
    actual = M6502.Add(-10, 47);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);

    /**FOR OPPOSITE-SIGN, NEGATIVE INTEGERS*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 1, 0, 0, 0, 0, 0, 1 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 0, 0, 0, 1 };
    expected.number = -19;

    /** When: */
    actual = M6502.Add(-30, 10);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(Binary_Addition_Tests, Test_Signed_No_Carry)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;

    /**---------------SIGNED NUMBER ADDITION WITHOUT CARRY-----------------*/
    /**FOR POSITIVE INTEGERS, SAME AS UNSIGNED ADDITION WITHOUT CARRY*/
    /**FOR SAME-SIGN, NEGATIVE INTEGERS*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 1 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 1, 0, 0, 0, 0, 0, 1 };
    expected.number = -64;

    /** When: */
    actual = M6502.Add(-60, -4);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(Binary_Addition_Tests, Test_Signed_Overflow)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;

    /**--------------SIGNED NUMBERS OVERFLOW-----------------------------*/
    /**POSITIVE SIGNED NUMBERS*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 0, 0, 1, 1 };

    /** When: */
    actual = M6502.Add(+60, +70);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);
    /**NEGATIVE SIGNED NUMBERS*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 1, 0, 0, 0, 0, 1, 0 };

    /** When: */
    actual = M6502.Add(-60, -70);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(Binary_Addition_Tests, Test_Opposite_Signs)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;

    /**--------------OPPOSITE NUMBERS ADDITION-----------------------------*/
    /**POSITIVE SIGNED NUMBERS*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 1, 1, 0, 0, 0, 0, 0 };

    /** When: */
    actual = M6502.Add(+60, -60);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(BCD_Addition_Tests, Test_Without_Carry)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;

    /**WITHOUT EXTERNAL OR INTERNAL CARRY*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 1, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 1, 0, 0, 0 };
    expected.number = 0x21;

    /** When: */
    actual = M6502.Add(0x10, 0x11);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(BCD_Addition_Tests, Test_Without_External_Carry)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;

    /**WITHOUT EXTERNAL CARRRY*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 1, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 1, 0, 0, 0 };
    expected.number = 0x10;

    /** When: */
    actual = M6502.Add(0x9, 0x1);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);

    /**WITH ALL CARRRIES*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 1, 0, 0, 1, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 1, 0, 0, 0 };
    expected.number = 0x11;

    /** When: */
    actual = M6502.Add(0x9, 0x1);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);

    /**PRODUCING CARRY*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 1, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 1, 1, 0, 1, 0, 0, 0 };
    expected.number = 0x00;

    /** When: */
    actual = M6502.Add(0x99, 0x1);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(BCD_Addition_Tests, Test_With_Carries)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    
    /**WITH ALL CARRRIES*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 1, 0, 0, 1, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 1, 0, 0, 0 };
    expected.number = 0x11;

    /** When: */
    actual = M6502.Add(0x9, 0x1);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(BCD_Addition_Tests, Test_Producing_Carry)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    
    /**PRODUCING CARRY*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 1, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 1, 1, 0, 1, 0, 0, 0 };
    expected.number = 0x00;

    /** When: */
    actual = M6502.Add(0x99, 0x1);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);
}
