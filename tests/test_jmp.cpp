#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(JMP_Tests, Test_JMP_ABS)
{
    /**
     * Test the Jump to subroutine instruction
    */
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------ABSOLUTE ADDRESS------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)JMP_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);

    /** When: */
    clk.SetInstructionStep(1);
    clk.StartCounting();
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PC, 0x4242);
    EXPECT_EQ(cycles, 3);
}

TEST(JMP_Tests, Test_JMP_ICT)
{
    /**
     * Test the Jump to subroutine instruction
    */
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------INDIRECT ADDRESS------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)JMP_IND);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4242, (BYTE)0x21);
    mem.DebugWrite(0x4242 + 1, (BYTE)0x21);

    /** When: */
    clk.SetInstructionStep(1);
    clk.StartCounting();
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PC, 0x2121);
    EXPECT_EQ(cycles, 5);
}
