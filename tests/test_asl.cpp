#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(ASL_Tests, Test_ASL_A)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------------------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)ASL);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.A, (BYTE)(0xFF<<1));
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(M6502.RegisterFile.PS.C, 1);
    EXPECT_EQ(cycles, 2);
}

TEST(ASL_Tests, Test_ASL_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD A FROM ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)ASL_ZP);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);
    mem.DebugWrite(0x1, (BYTE)0xFF);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x1), (BYTE)(0xFF<<1));
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(M6502.RegisterFile.PS.C, 1);
    EXPECT_EQ(cycles, 5);
}

TEST(ASL_Tests, Test_ASL_ZP_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD A FROM ZERO PAGE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)ASL_ZP_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);
    mem.DebugWrite(0x11, (BYTE)0xFF);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x11), (BYTE)(0xFF<<1));
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(M6502.RegisterFile.PS.C, 1);
    EXPECT_EQ(cycles, 6);
}

TEST(ASL_Tests, Test_ASL_ABS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD A FROM ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)ASL_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4242, (BYTE)0xFF);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4242), (BYTE)(0xFF<<1));
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(M6502.RegisterFile.PS.C, 1);
    EXPECT_EQ(cycles, 6);
}

TEST(ASL_Tests, Test_ASL_ABS_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD A FROM ABSOLUTE + X-----------------*/
    /**-------CASE WITHOUT PAGE CROSS---------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0xFF;
    M6502.RegisterFile.X = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)ASL_ABS_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4262, (BYTE)0xFF);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4262), (BYTE)(0xFF<<1));
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(M6502.RegisterFile.PS.C, 1);
    EXPECT_EQ(cycles, 7);

    /**-------CASE WITH PAGE CROSS---------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0xFF;
    M6502.RegisterFile.X = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)ASL_ABS_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4242 + 0xFF, (BYTE)0xFF);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4242 + 0xFF), (BYTE)(0xFF<<1));
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(M6502.RegisterFile.PS.C, 1);
    EXPECT_EQ(cycles, 7);
}
