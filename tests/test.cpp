#include "gtest/gtest.h"
#include <math.h>
#include "../CPU.h"

TEST(M6502Tests, BitTest_Tests)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    M6502.Reset(mem);

    /** Given: */
    start.flags = { 0, 0, 0, 1, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 1, 0, 0, 0 };
    expected.number = 0x10;

    /** When: */
    actual = M6502.BitTest(0x10, 0x11);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);

    /** Given: */
    start.flags = { 0, 0, 0, 1, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 1, 0, 1, 0, 0, 0 };
    expected.number = 0x0;

    /** When: */
    actual = M6502.BitTest(0x10, 0x1);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);

    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    expected.flags = { 0, 0, 0, 0, 0, 1, 1 };
    expected.number = 0xC0;

    /** When: */
    actual = M6502.BitTest(0xC0, 0xC0);

    /** Then: */
    EXPECT_EQ(actual.number, expected.number);
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(M6502Tests, Compare_Tests)
{
    /**
     * Comparison on 6502 works only for unsigned integers.
     * Source: https://www.atariarchives.org/alp/appendix_1.php
     * THIS TEST CASE USES DECIMAL NUMBERS INSTEAD OF HEX!
     * 
     * ACCU - MEM = Z, C, N
     * 
     * Z = 1 if ACCU = MEM
     * N = 1 if ACCU < MEM
     * C = 1 if ACCU >= MEM
     * 
    */
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    /**---------------ACCU = MEM-----------------*/
    const int OPB = 26;
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    M6502.RegisterFile.A = 26;
    expected.flags = { 1, 1, 0, 0, 0, 0, 0 };

    /** When: */
    actual = M6502.Compare(M6502.RegisterFile.A, OPB);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);

    /**---------------ACCU > MEM-----------------*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    M6502.RegisterFile.A = (BYTE)48;
    expected.flags = { 1, 0, 0, 0, 0, 0, 0 };

    /** When: */
    actual = M6502.Compare(M6502.RegisterFile.A, OPB);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);

    /**---------------ACCU > MEM & RES > 0-----------------*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 1 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    M6502.RegisterFile.A = (BYTE)130;
    expected.flags = { 1, 0, 0, 0, 0, 0, 0 };

    /** When: */
    actual = M6502.Compare(M6502.RegisterFile.A, OPB);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);

    /**---------------ACCU <= MEM-----------------*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    M6502.RegisterFile.A = (BYTE)8;
    expected.flags = { 0, 0, 0, 0, 0, 0, 1 };

    /** When: */
    actual = M6502.Compare(M6502.RegisterFile.A, OPB);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);
}

TEST(M6502Tests, ExclusiveOR_Tests)
{
    
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    /**------------------------------------------*/
    int OPB = 0x9D;
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    M6502.RegisterFile.A = 0x5C;
    expected.flags = { 0, 0, 0, 0, 0, 0, 1 };

    /** When: */
    actual = M6502.ExclusiveOR(M6502.RegisterFile.A, OPB);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);
    EXPECT_EQ(actual.number, 0xC1);

    /**------------------------------------------*/
    OPB = 0x7A;
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    M6502.RegisterFile.A = 0x5C;
    expected.flags = { 0, 0, 0, 0, 0, 0, 0 };

    /** When: */
    actual = M6502.ExclusiveOR(M6502.RegisterFile.A, OPB);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);
    EXPECT_EQ(actual.number, 0x26);

    /**------------------------------------------*/
    OPB = 0x5C;
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    M6502.RegisterFile.A = 0x5C;
    expected.flags = { 0, 1, 0, 0, 0, 0, 0 };

    /** When: */
    actual = M6502.ExclusiveOR(M6502.RegisterFile.A, OPB);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);
    EXPECT_EQ(actual.number, 0x0);
}

TEST(M6502Tests, RightShift_Tests)
{
    CPU::OP_RESULT start, actual, expected;
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    /**---------------------------------------------------*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    M6502.RegisterFile.A = 1;
    expected.flags = { 1, 1, 0, 0, 0, 0, 0 };

    /** When: */
    actual = M6502.RightShift(M6502.RegisterFile.A);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);
    EXPECT_EQ(expected.number, 0);
    /**---------------------------------------------------*/
    M6502.Reset(mem);
    /** Given: */
    start.flags = { 0, 0, 0, 0, 0, 0, 0 };
    M6502.RegisterFile.PS.FromByte(start.flags.ToByte());
    M6502.RegisterFile.A = 2;
    expected.flags = { 0, 0, 0, 0, 0, 0, 0 };
    expected.number = 1;

    /** When: */
    actual = M6502.RightShift(M6502.RegisterFile.A);

    /** Then: */
    EXPECT_EQ(actual.flags, expected.flags);
    EXPECT_EQ(actual.number, expected.number);
}