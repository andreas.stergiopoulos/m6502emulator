#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(STY_Tests, Test_STY_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------STORE Y TO ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)STY_ZP);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0xD0), 0x42);
    EXPECT_EQ(cycles, 3);
}

TEST(STY_Tests, Test_STY_ZP_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------STORE Y TO ZERO PAGE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x42;
    M6502.RegisterFile.X = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)STY_ZP_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x11), 0x42);
    EXPECT_EQ(cycles, 4);
}

TEST(STY_Tests, Test_STY_ABS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------STORE Y TO ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)STY_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4242), 0x42);
    EXPECT_EQ(cycles, 4);
}
