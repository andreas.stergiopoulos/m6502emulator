#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(TXS_Tests, Test_TXS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------TRANSFER X TO STACK-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)TXS);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, M6502.RegisterFile.SP);
    EXPECT_EQ(cycles, 2);
}
