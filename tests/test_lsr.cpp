#include "gtest/gtest.h"
#include <math.h>
#include "../CPU.h"

TEST(LSR_Tests, Test_LSR_A)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------LOAD Y WITH IMMEDIATE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)LSR_A);
    M6502.RegisterFile.A = 0xD0;

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.A, 0x68);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 2);
}

TEST(LSR_Tests, Test_LSR_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LSR FROM ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)LSR_ZP);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xD0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0xD0), 0x21);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 5);
}

TEST(LSR_Tests, Test_LSR_ZP_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LSR FROM ZERO PAGE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)LSR_ZP_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);
    mem.DebugWrite(0x11, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x11), 0x21);
    EXPECT_EQ(cycles, 6);
}

TEST(LSR_Tests, Test_LSR_ABS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------DEC FROM ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)LSR_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4242, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4242), 0x21);
    EXPECT_EQ(cycles, 6);
}

TEST(LSR_Tests, Test_LSR_ABS_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------DEC A FROM ABSOLUTE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)LSR_ABS_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4262, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4262), 0x21);
    EXPECT_EQ(cycles, 7);
}
