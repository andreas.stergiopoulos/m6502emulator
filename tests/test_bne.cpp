#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(BNE_Tests, Test_Branch_Succeeds)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**----------------BRANCH SUCCEEDS-----------------*/
    /**PAGE IS NOT CROSSED*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.PC = 0x1000;
    M6502.RegisterFile.PS.Z = false;
    mem.DebugWrite(0x1000, (BYTE)BNE);
    mem.DebugWrite(0x1000 + 1, (BYTE)0x90);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PC, (ADDRESS)(0x1092));
    EXPECT_EQ(cycles, 3);

    /**PAGE IS CROSSED*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.PC = 0x107F;
    M6502.RegisterFile.PS.Z = false;
    mem.DebugWrite(0x107F, (BYTE)BNE);
    mem.DebugWrite(0x107F + 1, (BYTE)0xFF);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PC, (ADDRESS)(0x1180));
    EXPECT_EQ(cycles, 5);
}

TEST(BNE_Tests, Test_Branch_Fails)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------BRANCH DOESN'T SUCCEED-----------------*/
    /**PAGE IS CROSSED*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.PC = 0x1000;
    M6502.RegisterFile.PS.Z = true;
    mem.DebugWrite(0x1000, (BYTE)BNE);
    mem.DebugWrite(0x1000 + 1, (BYTE)0x90);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PC, (ADDRESS)(0x1000 + 2));
    EXPECT_EQ(cycles, 2);
}
