#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(BRK_Tests, Test_BRK)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.PC = 0x1000;
    mem.DebugWrite(0x1000, (BYTE)BRK);
    mem.DebugWrite(0xFFFE, (ADDRESS)0xFCE2);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS.B, 1);
    EXPECT_EQ(M6502.RegisterFile.PC, (ADDRESS)0xFCE2);
    EXPECT_EQ(cycles, 7);
}
