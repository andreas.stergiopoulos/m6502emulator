#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(PLP_Tests, Test_PLP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------PULL PROGRAM STATUS FROM STACK-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.PS.N = 1;
    M6502.RegisterFile.PS.Z = 1;
    M6502.RegisterFile.PS.V = 1;
    mem.DebugWrite(0xFCE2, (BYTE)PHP);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)PLP);

    /** When: */
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    M6502.RegisterFile.PS.N = 0;
    M6502.RegisterFile.PS.Z = 0;
    M6502.RegisterFile.PS.V = 0;
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(cycles, 4);
    EXPECT_EQ(M6502.RegisterFile.PS.N, 1);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 1);
    EXPECT_EQ(M6502.RegisterFile.PS.V, 1);
}
