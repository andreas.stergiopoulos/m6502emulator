#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(TSX_Tests, Test_TSX)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------STORE SP TO X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.SP = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)TSX);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.SP, M6502.RegisterFile.X);
    EXPECT_EQ(cycles, 2);
}
