#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(PLA_Tests, Test_PLA)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------PULL A FROM STACK-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)PHA);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)PLA);

    /** When: */
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    M6502.RegisterFile.A = 0;
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(cycles, 4);
    EXPECT_EQ(M6502.RegisterFile.A, 0x42);
}
