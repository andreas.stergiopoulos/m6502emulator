#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

/**
 * Due to the fact that all comparison results have been tested in the according
 * testing suite, this suite only checks for the equality case. The goal of this 
 * test suite is to determine if the instruction operates correctly regarding the
 * timing and the address space.
*/

TEST(CMP_Tests, Test_CMP_IM)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP A WITH IMMEDIATE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_IM);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x10);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 2);
}

TEST(CMP_Tests, Test_CMP_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP A FROM ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_ZP);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);
    mem.DebugWrite(0x1, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 3);
}

TEST(CMP_Tests, Test_CMP_ZP_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP A FROM ZERO PAGE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.X = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_ZP_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);
    mem.DebugWrite(0x11, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 4);
}

TEST(CMP_Tests, Test_CMP_ABS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP A FROM ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4242, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 4);
}

TEST(CMP_Tests, Test_CMP_ABS_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP A FROM ABSOLUTE + X-----------------*/
    /**-------CASE WITHOUT PAGE CROSS---------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.X = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_ABS_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4262, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 4);

    /**-------CASE WITH PAGE CROSS---------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.X = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_ABS_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4242 + 0xFF, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 5);
}

TEST(CMP_Tests, Test_CMP_ABS_Y)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP A FROM ABSOLUTE + Y-----------------*/
    /**----------CASE WITHOUT PAGE CROSS------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.Y = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_ABS_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4262, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 4);

    /**----------CASE WITH PAGE CROSS------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.Y = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_ABS_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4242 + 0xFF, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 5);
}

TEST(CMP_Tests, Test_CMP_IXD_ICT)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP A FROM INDEXED INDIRECT-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.X = 0x4;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_IND_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x20);
    mem.DebugWrite(0x24, (BYTE)0x74);
    mem.DebugWrite(0x24 + 1, (BYTE)0x20);
    mem.DebugWrite(0x2074, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 6);

    /**---------------CMP A FROM INDIRECT INDEXED-----------------*/
    /**-----------CASE WITHOUT PAGE CROSS---------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.Y = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_IND_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x86);
    mem.DebugWrite(0x86, (BYTE)0x74);
    mem.DebugWrite(0x86 + 1, (BYTE)0x20);
    mem.DebugWrite(0x2094, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 5);

    /**-----------CASE WITH PAGE CROSS---------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.Y = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_IND_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x86);
    mem.DebugWrite(0x86, (BYTE)0x74);
    mem.DebugWrite(0x86 + 1, (BYTE)0x20);
    mem.DebugWrite(0x2074 + 0xFF, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 6);
}

TEST(CMP_Tests, Test_CMP_ICT_IXD)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    CPU::STATUS_FLAGS expected = { 1, 1, 0, 0, 0, 0, 0 };

    /**---------------CMP A FROM INDIRECT INDEXED-----------------*/
    /**-----------CASE WITHOUT PAGE CROSS---------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.Y = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_IND_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x86);
    mem.DebugWrite(0x86, (BYTE)0x74);
    mem.DebugWrite(0x86 + 1, (BYTE)0x20);
    mem.DebugWrite(0x2094, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 5);

    /**-----------CASE WITH PAGE CROSS---------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    M6502.RegisterFile.Y = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)CMP_IND_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x86);
    mem.DebugWrite(0x86, (BYTE)0x74);
    mem.DebugWrite(0x86 + 1, (BYTE)0x20);
    mem.DebugWrite(0x2074 + 0xFF, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PS, expected);
    EXPECT_EQ(cycles, 6);
}
