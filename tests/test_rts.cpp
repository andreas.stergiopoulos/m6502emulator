#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(RTS_Tests, Test_RTS)
{
    /**
     * Test the Jump to subroutine instruction
    */
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)RTS);
    M6502.PushAddressToStack(clk, mem, 0x4342-1);

    /** When: */
    clk.SetInstructionStep(1);
    clk.StartCounting();
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PC, 0x4342);
    EXPECT_EQ(cycles, 6);
}
