#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(CLD_Tests, Test_CLD)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------LOAD Y WITH IMMEDIATE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.PS.D = 1;
    mem.DebugWrite(0xFCE2, (BYTE)CLD);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(cycles, 2);
    EXPECT_EQ(M6502.RegisterFile.PS.D, 0);
}
