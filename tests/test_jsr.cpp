#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(JSR_Tests, Test_JSR_IM)
{
    /**
     * Test the Jump to subroutine instruction
    */
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)JSR);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);

    /** When: */
    clk.SetInstructionStep(1);
    clk.StartCounting();
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.PC, 0x4242);
    EXPECT_EQ(cycles, 6);
}
