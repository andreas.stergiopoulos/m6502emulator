#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(INY_Tests, Test_INY_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    
    /**---------------INY ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x4;
    mem.DebugWrite(0xFCE2, (BYTE)INY);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.Y, 0x5);
    EXPECT_EQ(cycles, 2);
}
