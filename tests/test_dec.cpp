#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(DEC_Tests, Test_DEC_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    
    /**---------------DEC FROM ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)DEC_ZP);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);
    mem.DebugWrite(0x1, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x1), 0x41);
    EXPECT_EQ(cycles, 5);
}

TEST(DEC_Tests, Test_DEC_ZP_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------ADC A FROM ZERO PAGE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)DEC_ZP_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x1);
    mem.DebugWrite(0x11, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x11), 0x41);
    EXPECT_EQ(cycles, 6);
}

TEST(DEC_Tests, Test_DEC_ABS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------DEC FROM ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)DEC_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4242, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4242), 0x41);
    EXPECT_EQ(cycles, 6);
}

TEST(DEC_Tests, Test_DEC_ABS_X)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------DEC A FROM ABSOLUTE + X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x20;
    mem.DebugWrite(0xFCE2, (BYTE)DEC_ABS_X);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0x42);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x4262, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(mem.DebugRead(0x4262), 0x41);
    EXPECT_EQ(cycles, 7);
}
