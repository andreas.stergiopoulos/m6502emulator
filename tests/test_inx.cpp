#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(INX_Tests, Test_INX_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    
    /**---------------DEC FROM ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.X = 0x4;
    mem.DebugWrite(0xFCE2, (BYTE)INX);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0x5);
    EXPECT_EQ(cycles, 2);
}
