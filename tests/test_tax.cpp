#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(TAX_Tests, Test_TAX)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------STORE A TO X-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)TAX);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, M6502.RegisterFile.A);
    EXPECT_EQ(cycles, 2);
}
