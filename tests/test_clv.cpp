#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(CLV_Tests, Test_CLV)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------CLEAR OVERFLOW FLAG-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.PS.V = 1;
    mem.DebugWrite(0xFCE2, (BYTE)CLV);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(cycles, 2);
    EXPECT_EQ(M6502.RegisterFile.PS.V, 0);
}
