#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(PHA_Tests, Test_PHA)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------PUSH A TO STACK-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)PHA);
    M6502.RegisterFile.A = 0x42;

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(cycles, 3);
    EXPECT_EQ(mem.DebugRead(M6502.RegisterFile.SP+0x100+1), 0x42);
}
