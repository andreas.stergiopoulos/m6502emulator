#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(LDX_Tests, Test_LDX_IM)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------LOAD X WITH IMMEDIATE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)LDX_IM);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0xD0);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 2);
}

TEST(LDX_Tests, Test_LDX_ZP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD X FROM ZERO PAGE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)LDX_ZP);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xD0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 3);
}

TEST(LDX_Tests, Test_LDX_ZP_Y)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD X FROM ZERO PAGE + Y-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)LDX_ZP_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xE0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 4);

    /**---------------LOAD X FROM ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)LDX_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x42D0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 4);

    /**---------------LOAD X FROM ABSOLUTE + Y-----------------*/
    /**-------SUBCASE WITHOUT PAGE CROSS-------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)LDX_ABS_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x42E0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 4);

    /**-------SUBCASE WITH PAGE CROSS-------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)LDX_ABS_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x43CF, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 5);
}

TEST(LDX_Tests, Test_LDX_ZP_ABS)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD X FROM ABSOLUTE-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)LDX_ABS);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x42D0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 4);
}

TEST(LDX_Tests, Test_LDX_ZP_ABS_Y)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;

    /**---------------LOAD X FROM ABSOLUTE + Y-----------------*/
    /**-------SUBCASE WITHOUT PAGE CROSS-------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0x10;
    mem.DebugWrite(0xFCE2, (BYTE)LDX_ABS_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x42E0, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 4);

    /**-------SUBCASE WITH PAGE CROSS-------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.Y = 0xFF;
    mem.DebugWrite(0xFCE2, (BYTE)LDX_ABS_Y);
    mem.DebugWrite(0xFCE2 + 1, (BYTE)0xD0);
    mem.DebugWrite(0xFCE2 + 2, (BYTE)0x42);
    mem.DebugWrite(0x43CF, (BYTE)0x42);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.X, 0x42);
    EXPECT_EQ(M6502.RegisterFile.PS.Z, 0);
    EXPECT_EQ(cycles, 5);
}
