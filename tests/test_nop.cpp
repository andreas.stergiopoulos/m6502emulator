#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(NOP_Tests, Test_NOP)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------NOP-----------------*/
    /** Given: */
    M6502.Reset(mem);
    mem.DebugWrite(0xFCE2, (BYTE)NOP);

    /** When: */
    CPU::REGISTER_FILE reg_start = M6502.RegisterFile;
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(cycles, 2);
    EXPECT_EQ(reg_start, M6502.RegisterFile);
}
