#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(SEC_Tests, Test_SEC)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------SET CARRY FLAG-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.PS.C = 0;
    mem.DebugWrite(0xFCE2, (BYTE)SEC);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(cycles, 2);
    EXPECT_EQ(M6502.RegisterFile.PS.C, 1);
}
