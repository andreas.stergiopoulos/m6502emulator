#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(TAY_Tests, Test_TAY)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------STORE A TO Y-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.A = 0x42;
    mem.DebugWrite(0xFCE2, (BYTE)TAY);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(M6502.RegisterFile.Y, M6502.RegisterFile.A);
    EXPECT_EQ(cycles, 2);
}
