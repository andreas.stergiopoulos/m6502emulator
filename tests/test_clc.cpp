#include <gtest/gtest.h>
#include <math.h>
#include "../CPU.h"

TEST(CLC_Tests, Test_CLC)
{
    Memory mem(pow(2, 16));
    Clock clk;
    CPU M6502;
    uint64_t cycles;
    /**---------------CLEAR CARRY FLAG-----------------*/
    /** Given: */
    M6502.Reset(mem);
    M6502.RegisterFile.PS.C = 1;
    mem.DebugWrite(0xFCE2, (BYTE)CLC);

    /** When: */
    clk.StartCounting();
    clk.SetInstructionStep(1);
    M6502.Execute(clk, mem);
    cycles = clk.GetElapsedTime();

    /** Then: */
    EXPECT_EQ(cycles, 2);
    EXPECT_EQ(M6502.RegisterFile.PS.C, 0);
}
