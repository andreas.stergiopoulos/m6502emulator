#ifndef OPCODES_H
#define OPCODES_H

enum OPCODES
{
    /**
     * ADC : Add with Carry
    */
    ADC_IM = 0x69,
    ADC_ZP = 0x65,
    ADC_ZP_X = 0x75,
    ADC_ABS = 0x6D,
    ADC_ABS_X = 0x7D,
    ADC_ABS_Y = 0x79,
    ADC_IND_X = 0x61,
    ADC_IND_Y = 0x71,
    /**
     * AND : Logical And of accumulator with memory content
    */
    AND_IM = 0x29,
    AND_ZP = 0x25,
    AND_ZP_X = 0x35,
    AND_ABS = 0x2D,
    AND_ABS_X = 0x3D,
    AND_ABS_Y = 0x39,
    AND_IND_X = 0x21,
    AND_IND_Y = 0x31,
    /**
     * ASL : Arithmetic shift left of accumulator or memory content
    */
    ASL = 0x0A,
    ASL_ZP = 0x06,
    ASL_ZP_X = 0x16,
    ASL_ABS = 0x0E,
    ASL_ABS_X = 0x1E,
    /**
     * BCC : Branch if Carry Clear
    */
    BCC = 0x90,
    /**
     * BCS : Branch if Carry Set
    */
    BCS = 0xB0,
    /**
     * BEQ : Branch if EQual (Zero flag is set).
    */
    BEQ = 0xF0,
    /**
     * BIT : Test if one or more bits are set on a memory location.
    */
    BIT_ZP = 0x24,
    BIT_ABS = 0x2C,
    /**
     * BMI : Branch if MInus (Negative flag is set).
    */
    BMI = 0x30,
    /**
     * BNE : Branch Not Equal (Zero flag is clear).
    */
    BNE = 0xD0,
    /**
     * BPL : Branch if Positive (Negative flag is clear).
    */
    BPL = 0x10,
    /**
     * BRK : Break
    */
    BRK = 0x00,
    /**
     * BVC : Branch if oVerflow Clear.
    */
    BVC = 0x50,
    /**
     * BVS : Branch if oVerflow Set.
    */
    BVS = 0x70,
    /**
     * CLC : CLear Carry flag.
    */
    CLC = 0x18,
    /**
     * CLD : CLear Decimal flag.
    */
    CLD = 0xD8,
    /**
     * CLI : CLear Interrupt flag.
    */
    CLI = 0x58,
    /**
     * CLV : CLear oVerflow flag.
    */
    CLV = 0xB8,
    /**
     * CMP : CoMPare.
    */
    CMP_IM = 0xC9,
    CMP_ZP = 0xC5,
    CMP_ZP_X = 0xD5,
    CMP_ABS = 0xCD,
    CMP_ABS_X = 0xDD,
    CMP_ABS_Y = 0xD9,
    CMP_IND_X = 0xC1,
    CMP_IND_Y = 0xD1,
    /**
     * CPX : ComPare X register.
    */
    CPX_IM = 0xE0,
    CPX_ZP = 0xE4,
    CPX_ABS = 0xEC,
    /**
     * CPY : ComPare Y register.
    */
    CPY_IM = 0xC0,
    CPY_ZP = 0xC4,
    CPY_ABS = 0xCC,
    /**
     * DEC : DECrement. (RMW)
    */
    DEC_ZP = 0xC6,
    DEC_ZP_X = 0xD6,
    DEC_ABS = 0xCE,
    DEC_ABS_X = 0xDE,
    /**
     * DEX : DEcrement X.
    */
    DEX = 0xCA,
    /**
     * DEY : DEcrement Y.
    */
    DEY = 0x88,
    /**
     * EOR : Exclusive OR (R)
    */
    EOR_IM = 0x49,
    EOR_ZP = 0x45,
    EOR_ZP_X = 0x55,
    EOR_ABS = 0x4D,
    EOR_ABS_X = 0x5D,
    EOR_ABS_Y = 0x59,
    EOR_IND_X = 0x41,
    EOR_IND_Y = 0x51,
    /**
     * INC : INCrement. (RMW)
    */
    INC_ZP = 0xE6,
    INC_ZP_X = 0xF6,
    INC_ABS = 0xEE,
    INC_ABS_X = 0xFE,
    /**
     * INX : INcrement X.
    */
    INX = 0xE8,
    /**
     * INY : INcrement Y.
    */
    INY = 0xC8,
    /**
     * JMP : JuMP to address.
    */
    JMP_ABS = 0x4C,
    JMP_IND = 0x6C,
    /**
     * JSR : Jump to SubRoutine
    */
    JSR = 0x20, //Immediate
    /**
     * LDA : Load A register
    */
    LDA_IM = 0xA9,    //Immediate
    LDA_ZP = 0xA5,    //Zero Page
    LDA_ZP_X = 0xB5,  //Zero Page + X
    LDA_ABS = 0xAD,   //Absolute Address
    LDA_ABS_X = 0xBD, //Absolute + X, take 1 cycle more if addition produces carry.
    LDA_ABS_Y = 0xB9, //Absolute + Y, take 1 cycle more if addition produces carry.
    LDA_IND_X = 0xA1, //Indirect Address + X.
    LDA_IND_Y = 0xB1, //Indirect Address + Y, take 1 cycle more if addition produces carry.
    /**
     * LDX : Load X register
    */
    LDX_IM = 0xA2,    //Immediate
    LDX_ZP = 0xA6,    //Zero Page
    LDX_ZP_Y = 0xB6,  //Zero Page + Y
    LDX_ABS = 0xAE,   //Absolute Address
    LDX_ABS_Y = 0xBE, //Absolute Address + Y
    /**
     * LDY : Load Y register
    */
    LDY_IM = 0xA0,    //Immediate
    LDY_ZP = 0xA4,    //Zero Page
    LDY_ZP_X = 0xB4,  //Zero Page + X
    LDY_ABS = 0xAC,   //Absolute Address
    LDY_ABS_X = 0xBC, //Absolute Address + X
    /**
     * LSR : Logical Shift Right
    */
    LSR_A = 0x4A,
    LSR_ZP = 0x46,
    LSR_ZP_X = 0x56,
    LSR_ABS = 0x4E,
    LSR_ABS_X = 0x5E,
    /**
     * NOP : No Operation
    */
    NOP = 0xEA, //Just don't do anything
    /**
     * PHA : Push Accumulator to Stack
    */
    PHA = 0x48,
    /**
     * PHP : Push Processor Status to Stack
    */
    PHP = 0x08,
    /**
     * PLA : Pull From Stack to Accumulator
    */
    PLA = 0x68,
    /**
     * PLP : Pull From Stack to Processor Status Register
    */
    PLP = 0x28,
    /**
     * RTI : ReTurn from Interrupt
    */
    RTI = 0x40,
    /**
     * RTS : ReTurn from Subroutine
    */
    RTS = 0x60,
    /**
     * SEC : Set Carry Flag on Program Status Register
    */
    SEC = 0x38,
    /**
     * SED : Set Decimal Flag on Program Status Register
    */
    SED = 0xF8,
    /**
     * SEI : Set Interrupt Disable Flag on Program Status Register
    */
    SEI = 0x78,
    /**
     * STA : Store Accumulator
    */
    STA_ZP = 0x85,
    STA_ZP_X = 0x95,
    STA_ABS = 0x8D,
    STA_ABS_X = 0x9D,
    STA_ABS_Y = 0x99,
    STA_IND_X = 0x81,
    STA_IND_Y = 0x91,
    /**
     * STX : Store X Register
    */
    STX_ZP = 0x86,
    STX_ZP_Y = 0x96,
    STX_ABS = 0x8E,
    /**
     * STY : Store Y Register
    */
    STY_ZP = 0x84,
    STY_ZP_X = 0x94,
    STY_ABS = 0x8C,
    /**
     * TAX : Transfer A to X
    */
    TAX = 0xAA,
    /**
     * TAY : Transfer A to Y
    */
    TAY = 0xA8,
    /**
     * TSX : Transfer Stack Pointer to X
    */
    TSX = 0xBA,
    /**
     * TXA : Transfer X to Accumulator
    */
    TXA = 0x8A,
    /**
     * TXS : Transfer X to Stack Pointer
    */
    TXS = 0x9A,
    /**
     * TYA : Transfer Y to Accumulator
    */
    TYA = 0x98
};

#endif