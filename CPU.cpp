#include "CPU.h"

/*Fetch a byte from memory, incrementing the program counter.*/
BYTE CPU::FetchByte(Clock &clk, Memory &mem)
{
    /** The PC is incremented AFTER fetching the content. */
    clk.IncrementTick(1);
    return mem.ReadByteFromAddress(RegisterFile.PC++);
}

/*Fetch a word from memory using the Program Counter. 6502 is little endian.*/
ADDRESS CPU::FetchAddress(Clock &clk, Memory &mem)
{
    BYTE Values[2];
    clk.IncrementTick(1);
    Values[0] = mem.ReadByteFromAddress(RegisterFile.PC++);
    clk.IncrementTick(1);
    Values[1] = mem.ReadByteFromAddress(RegisterFile.PC++);
    return (Values[1] << 8) | (Values[0]);
}

/*Write a word in a location in memory.*/
void CPU::WriteAddress(Clock &clk, Memory &mem, ADDRESS address, ADDRESS word)
{
    clk.IncrementTick(2);
    mem.WriteAddressToAddress(address, word);
}

/*Write a word in a location in memory.*/
void CPU::WriteByte(Clock &clk, Memory &mem, ADDRESS address, BYTE byte)
{
    clk.IncrementTick(1);
    mem.WriteByteToAddress(address, byte);
}

/*Read a byte from memory without incrementing the program counter.*/
BYTE CPU::ReadByte(Clock &clk, Memory &mem, ADDRESS address)
{
    clk.IncrementTick(1);
    return mem.ReadByteFromAddress(address);
}

/*Fetch a word from memory without incrementing the Program Counter. 6502 is little endian.*/
ADDRESS CPU::ReadAddress(Clock &clk, Memory &mem, ADDRESS address)
{
    BYTE Values[2];
    clk.IncrementTick(1);
    Values[0] = mem.ReadByteFromAddress(address);
    clk.IncrementTick(1);
    Values[1] = mem.ReadByteFromAddress(++address);
    return (ADDRESS)(Values[1] << 8) + (Values[0]);
}

/** Push a word to the stack. The stack grows downwards. */
void CPU::PushAddressToStack(Clock &clk, Memory &mem, ADDRESS word)
{
    //NOTE: Should I check for stack overflow? Probably not.
    //NOTE: This is a bit smelly but I'll go with it.
    ADDRESS StackAddress;
    clk.IncrementTick(1);
    StackAddress = 0x100 + RegisterFile.SP;
    mem.WriteByteToAddress(StackAddress, (BYTE)(word & 0xFF));
    RegisterFile.SP--;
    clk.IncrementTick(1);
    StackAddress = 0x100 + RegisterFile.SP;
    mem.WriteByteToAddress(StackAddress, (BYTE)(word >> 8));
    RegisterFile.SP--;
}

ADDRESS CPU::PullAddressFromStack(Clock &clk, Memory &mem)
{
    BYTE Values[2];
    ADDRESS StackAddress;
    clk.IncrementTick(1);
    StackAddress = 0x100 + RegisterFile.SP;
    Values[0] = mem.ReadByteFromAddress(StackAddress);;
    RegisterFile.SP++;
    clk.IncrementTick(1);
    StackAddress = 0x100 + RegisterFile.SP;
    Values[1] = mem.ReadByteFromAddress(StackAddress);
    ADDRESS ReturnAddress = (ADDRESS)(Values[0] << 8) + (Values[1]);
    return ReturnAddress;
}

/** Update the value on register X. */
void CPU::UpdateX(BYTE value)
{
    RegisterFile.X = value;
    LDUpdatePS(RegisterFile.X);
}

/** Update the value on register Y. */
void CPU::UpdateY(BYTE value)
{
    RegisterFile.Y = value;
    LDUpdatePS(RegisterFile.Y);
}

/** Update the value on register A. */
void CPU::UpdateA(BYTE value)
{
    RegisterFile.A = value;
    LDUpdatePS(RegisterFile.A);
}

/*Update the Program Status register after executing an LD_ opcode.*/
void CPU::LDUpdatePS(BYTE value)
{
    RegisterFile.PS.N = (value & 0b10000000);
    RegisterFile.PS.Z = (value == 0);
}

void CPU::Reset(Memory &mem)
{
    mem.WriteAddressToAddress(RESET_VECTOR, 0xFCE2);
    RegisterFile.PC = mem.ReadAddressFromAddress(RESET_VECTOR);
    /** Stack starts at 0x100 and ends at 0x1FF, 
     * so we initialize the stack pointer to 0x1FF
     * provided that the stack grows from high to
     * low.
     * 
     * During the reset sequence, the processor performs 
     * 3 empth pushes to the stack, thus the stack pointer 
     * after the reset is set is not 0xFF, but oxFD instead.
     */
    RegisterFile.SP = 0xFD;
    RegisterFile.PS.C = 0;
    RegisterFile.PS.Z = 0;
    RegisterFile.PS.I = 0;
    RegisterFile.PS.D = 0;
    RegisterFile.PS.B = 0;
    RegisterFile.PS.V = 0;
    RegisterFile.PS.N = 0;
    RegisterFile.A = 0;
    RegisterFile.X = 0;
    RegisterFile.Y = 0;
    mem.InitializeMemory();
}

bool CPU::PageCrossDetection(ADDRESS Address, BYTE Offset)
{
    return ((Address & 0xFF) + Offset) > 0xFF;
}

BYTE CPU::AMIndexedIndirect(Clock &clk, Memory &mem)
{
    return ReadByte(clk, mem, AMGetIndexedIndirectAddress(clk, mem));
}

BYTE CPU::AMIndirectIndexed(Clock &clk, Memory &mem)
{
    return ReadByte(clk, mem, AMGetIndirectIndexedAddress(clk, mem, false));
}

ADDRESS CPU::AMGetAbsoluteAddress(Clock &clk, Memory &mem)
{
    ADDRESS AbsoluteAddress = FetchAddress(clk, mem);
    return AbsoluteAddress;
}

ADDRESS CPU::AMGetAbsoluteIndexedAddress(Clock &clk, Memory &mem, BYTE offset, bool PenaltyCycle)
{
    /** Read the Absolute Address from the memory. */
    ADDRESS AbsoluteAddress = FetchAddress(clk, mem);
    /**
     * Page cross detection: If the sum Absolute + X causes
     * an overflow of the lowest byte, we must add a cycle
     * to the total cycles of the operation because we have 
     * to propagate the carry to the next byte, utilizing thus 
     * the adder one more time.
     * 
     * Here, we basically have to check if the lowest byte before 
     * the addition is lower than the lowest byte after the addition.
     * If it is not, an overflow has occured.
    */
    if (PenaltyCycle || PageCrossDetection(AbsoluteAddress, offset))
    {
        clk.IncrementTick(1);
    }
    /** Calculate the new address by adding the offset. */
    AbsoluteAddress += offset;
    /** Read the data. */
    return AbsoluteAddress;
}

ADDRESS CPU::AMGetZeroPageAddress(Clock &clk, Memory &mem)
{
    BYTE ZeroPageAddress = FetchByte(clk, mem);
    return ZeroPageAddress;
}

ADDRESS CPU::AMGetZeroPageIndexedAddress(Clock &clk, Memory &mem, BYTE offset)
{
    BYTE ZeroPageAddress = FetchByte(clk, mem);
    BYTE Address = (ZeroPageAddress + offset) & 0xFF;
    clk.IncrementTick(1);
    return Address;
}

ADDRESS CPU::AMGetIndexedIndirectAddress(Clock &clk, Memory &mem)
{
    /**
     * Fetch the immediate needed, add to X and wrap around the Zero Page.
     * The fetching and addition to X happes at the same cycle.
    */
    BYTE BaseAddress = FetchByte(clk, mem);
    clk.IncrementTick(1);
    ADDRESS ZeroPageAddress = (BaseAddress + CPU::RegisterFile.X) & 0xFF;
    /**
     * Read the final address from the location inside the Zero Page.
    */
    ADDRESS FinalAddress = ReadAddress(clk, mem, ZeroPageAddress);
    /**
     * Read the value byte, store to A and update PS
    */
    return FinalAddress;
}

ADDRESS CPU::AMGetIndirectIndexedAddress(Clock &clk, Memory &mem, bool PenaltyCycle)
{
    /**
     * Fetch the immediate that gives the Zero Page address.
    */
    BYTE ZeroPageAddress = FetchByte(clk, mem);
    /**
     * Read the address from the Zero Page.
    */
    ADDRESS PointerAddress = ReadAddress(clk, mem, ZeroPageAddress);
    /**
     * Add Y and check for page crossing.
    */
    if (PenaltyCycle || PageCrossDetection(PointerAddress, RegisterFile.Y))
    {
        clk.IncrementTick(1);
    }
    ADDRESS FinalAddress = PointerAddress + CPU::RegisterFile.Y;
    /**
     * Read the value at the address that we finally calculated.
    */
    return FinalAddress;
}

ADDRESS CPU::AMGetIndirectAddress(Clock &clk, Memory &mem)
{
    /**
     * Fetch the indirect address.
    */
    ADDRESS IndirectAddress = FetchAddress(clk, mem);
    /**
     * Read the address from the Zero Page.
    */
    ADDRESS TargetAddress = ReadAddress(clk, mem, IndirectAddress);
    /**
     * Return the value at the address that we finally calculated.
    */
    return TargetAddress;
}


ADDRESS CPU::AMGetRelativeAddress(Clock &clk, Memory &mem, BYTE Offset)
{
    ADDRESS Target;
    Target = RegisterFile.PC + Offset;
    if(PageCrossDetection(RegisterFile.PC, Offset))
    {
        clk.IncrementTick(2);
    }
    return Target;
}

/*Push a byte to the stack.*/
void CPU::PushByteToStack(Clock &clk, Memory &mem, BYTE byte)
{
    ADDRESS StackAddress = RegisterFile.SP + 0x100;
    mem.WriteByteToAddress(StackAddress, byte);
    RegisterFile.SP--;
    clk.IncrementTick(1);
}

/*Pull a byte from the stack.*/
BYTE CPU::PullByteFromStack(Clock &clk, Memory &mem)
{
    clk.IncrementTick(1);
    ++RegisterFile.SP;
    ADDRESS StackAddress = RegisterFile.SP + 0x100; //Add 0x100 because it's the offset of the second page.
    return ReadByte(clk, mem, StackAddress);
}

CPU::OP_RESULT CPU::Add(BYTE OpA, BYTE OpB)
{
    OP_RESULT result = {
        0,
        RegisterFile.PS
    };

    if (RegisterFile.PS.D == true)
    {
        result = BCDAdd(OpA, OpB);
    }
    else
    {
        uint8_t Res = 0;

        Res = OpA + OpB + RegisterFile.PS.C;
        result.number = (BYTE)Res;

        result.flags.C = Res < OpA;
        result.flags.N = ((result.number & 0b10000000) != 0x0 ? true : false);
        result.flags.V = ((int8_t)OpA > 0 && (int8_t)OpB > 0 && (int8_t)Res < 0) || 
            ((int8_t)OpA < 0 && (int8_t)OpB < 0 && (int8_t)Res > 0);
        result.flags.Z = (Res == 0);
    }
    return result;
}

CPU::OP_RESULT CPU::BCDAdd(BYTE OpA, BYTE OpB)
{
    OP_RESULT result;
    result = 
    {
        0,
        RegisterFile.PS
    };

    union BCD_Representation
    {
        BYTE AsByte;
        struct BCD
        {
            BYTE LowNibble : 4;
            BYTE HighNibble : 4;
        } AsBCD;
    };

    BCD_Representation A, B, Res;
    A.AsByte = OpA;
    B.AsByte = OpB;

    Res.AsByte = A.AsByte + B.AsByte + RegisterFile.PS.C;
    if (Res.AsBCD.LowNibble > 0x9)
    {
        Res.AsByte += 0x6;
    }

    if (Res.AsBCD.HighNibble > 9)
    {
        Res.AsByte += 0x6 << 4;
        result.flags.C = true;
    }

    result.flags.C = Res.AsByte < OpA;
    result.flags.N = ((result.number & 0b10000000) != 0x0 ? true : false);
    result.flags.Z = (Res.AsByte == 0);

    result.number = Res.AsByte;
    return result;
}

CPU::OP_RESULT CPU::BitTest(BYTE OpA, BYTE OpB)
{
    OP_RESULT result;
    result = 
    {
        0,
        RegisterFile.PS
    };

    result.number = (BYTE)(OpA & OpB);

    result.flags.Z = (result.number == 0x0);
    result.flags.N = (0x80 & OpB)>>7;
    result.flags.V = (0x40 & OpB)>>6;

    return result;
}

CPU::OP_RESULT CPU::Subtract(BYTE OpA, BYTE OpB)
{
    OP_RESULT result = {
        0,
        RegisterFile.PS
    };

    if (RegisterFile.PS.D == true)
    {
        result = BCDSubtract(OpA, OpB);
    }
    else
    {
        uint8_t Res = 0;

        Res = OpA + OpB + RegisterFile.PS.C;
        result.number = (BYTE)Res;

        result.flags.C = Res < OpA;
        result.flags.N = ((result.number & 0b10000000) != 0x0 ? true : false);
        result.flags.V = ((int8_t)OpA > 0 && (int8_t)OpB > 0 && (int8_t)Res < 0) || 
            ((int8_t)OpA < 0 && (int8_t)OpB < 0 && (int8_t)Res > 0);
        result.flags.Z = (Res == 0);
    }
    return result;
}

CPU::OP_RESULT CPU::BCDSubtract(BYTE OpA, BYTE OpB)
{
    OP_RESULT result;
    result = 
    {
        0,
        RegisterFile.PS
    };

    union BCD_Representation
    {
        BYTE AsByte;
        struct BCD
        {
            BYTE LowNibble : 4;
            BYTE HighNibble : 4;
        } AsBCD;
    };

    BCD_Representation A, B, Res;
    A.AsByte = OpA;
    B.AsByte = OpB;

    Res.AsByte = A.AsByte + B.AsByte + RegisterFile.PS.C;
    if (Res.AsBCD.LowNibble > 0x9)
    {
        Res.AsByte += 0x6;
    }

    if (Res.AsBCD.HighNibble > 9)
    {
        Res.AsByte += 0x6 << 4;
        result.flags.C = true;
    }

    result.flags.C = Res.AsByte < OpA;
    result.flags.N = ((result.number & 0b10000000) != 0x0 ? true : false);
    result.flags.Z = (Res.AsByte == 0);

    result.number = Res.AsByte;
    return result;
}

CPU::OP_RESULT CPU::Compare(BYTE OpA, BYTE OpB)
{
    OP_RESULT result;
    result = 
    {
        0,
        RegisterFile.PS
    };

    result.number = OpA - OpB;

    result.flags.Z = (result.number == 0);
    result.flags.N = (result.number & 0x80)>>7;
    result.flags.C = (OpA >= OpB);

    return result;
}

CPU::OP_RESULT CPU::ExclusiveOR(BYTE OpA, BYTE OpB)
{
    OP_RESULT result;
    result = 
    {
        0,
        RegisterFile.PS
    };

    result.number = OpA ^ OpB;

    result.flags.Z = (result.number == 0);
    result.flags.N = (result.number & 0x80)>>7;

    return result;
}

CPU::OP_RESULT CPU::RightShift(BYTE Operand)
{
    OP_RESULT result;
    result = 
    {
        0,
        RegisterFile.PS
    };

    result.number = Operand >> 1;
    // result.number = result.number + (RegisterFile.PS.C << 7);

    result.flags.Z = (result.number == 0);
    result.flags.N = (result.number & 0x80)>>7;
    result.flags.C = (Operand & 0x1);

    return result;
}

void CPU::Execute(Clock &clk, Memory &mem)
{
    while (clk.Proceed())
    {
        BYTE Instruction = this->FetchByte(clk, mem);
        clk.IncrementInstruction();
        switch (Instruction)
        {
        case ADC_IM:
        {
            BYTE Immediate = FetchByte(clk, mem);
            OP_RESULT Result = Add(RegisterFile.A, Immediate);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case ADC_ZP:
        {
            ADDRESS LoadAddress = AMGetZeroPageAddress(clk, mem);
            BYTE OperandB = ReadByte(clk, mem, LoadAddress);
            OP_RESULT Result = Add(RegisterFile.A, OperandB);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case ADC_ZP_X:
        {
            ADDRESS LoadAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE OperandB = ReadByte(clk, mem, LoadAddress);
            OP_RESULT Result = Add(RegisterFile.A, OperandB);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case ADC_ABS:
        {
            ADDRESS LoadAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE OperandB = ReadByte(clk, mem, LoadAddress);
            OP_RESULT Result = Add(RegisterFile.A, OperandB);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case ADC_ABS_X:
        {
            ADDRESS LoadAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, false);
            BYTE OperandB = ReadByte(clk, mem, LoadAddress);
            OP_RESULT Result = Add(RegisterFile.A, OperandB);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case ADC_ABS_Y:
        {
            ADDRESS LoadAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.Y, false);
            BYTE OperandB = ReadByte(clk, mem, LoadAddress);
            OP_RESULT Result = Add(RegisterFile.A, OperandB);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case ADC_IND_X:
        {
            ADDRESS LoadAddress = AMGetIndexedIndirectAddress(clk, mem);
            BYTE OperandB = ReadByte(clk, mem, LoadAddress);
            OP_RESULT Result = Add(RegisterFile.A, OperandB);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case ADC_IND_Y:
        {
            ADDRESS LoadAddress = AMGetIndirectIndexedAddress(clk, mem, false);
            BYTE OperandB = ReadByte(clk, mem, LoadAddress);
            OP_RESULT Result = Add(RegisterFile.A, OperandB);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case AND_IM:
        {
            BYTE Immediate = FetchByte(clk, mem);
            BYTE NewValue = RegisterFile.A & Immediate;
            UpdateA(NewValue);
        }
        break;
        case AND_ZP:
        {
            ADDRESS LoadAddress = AMGetZeroPageAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            BYTE NewValue = RegisterFile.A & Operand;
            UpdateA(NewValue);
        }
        break;
        case AND_ZP_X:
        {
            ADDRESS LoadAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            BYTE NewValue = RegisterFile.A & Operand;
            UpdateA(NewValue);
        }
        break;
        case AND_ABS:
        {
            ADDRESS LoadAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            BYTE NewValue = RegisterFile.A & Operand;
            UpdateA(NewValue);
        }
        break;
        case AND_ABS_X:
        {
            ADDRESS LoadAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, false);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            BYTE NewValue = RegisterFile.A & Operand;
            UpdateA(NewValue);
        }
        break;
        case AND_ABS_Y:
        {
            ADDRESS LoadAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.Y, false);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            BYTE NewValue = RegisterFile.A & Operand;
            UpdateA(NewValue);
        }
        break;
        case AND_IND_X:
        {
            ADDRESS LoadAddress = AMGetIndexedIndirectAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            BYTE NewValue = RegisterFile.A & Operand;
            UpdateA(NewValue);
        }
        break;
        case AND_IND_Y:
        {
            ADDRESS LoadAddress = AMGetIndirectIndexedAddress(clk, mem, false);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            BYTE NewValue = RegisterFile.A & Operand;
            UpdateA(NewValue);
        }
        break;
        case ASL:
        {
            RegisterFile.PS.C = ((0x80 & RegisterFile.A) >> 7);
            BYTE NewValue = RegisterFile.A << 1;
            UpdateA(NewValue);
            clk.IncrementTick(1);
        }
        break;
        case ASL_ZP:
        {
            ADDRESS LoadAddress = AMGetZeroPageAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            WriteByte(clk, mem, LoadAddress, Operand);
            RegisterFile.PS.C = ((0x80 & Operand) >> 7);
            Operand = Operand << 1;
            RegisterFile.PS.N = ((0x80 & Operand)>>7);
            RegisterFile.PS.Z = (Operand == 0);
            WriteByte(clk, mem, LoadAddress, Operand);
        }
        break;
        case ASL_ZP_X:
        {
            ADDRESS LoadAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            WriteByte(clk, mem, LoadAddress, Operand);
            RegisterFile.PS.C = ((0x80 & Operand) >> 7);
            Operand = Operand << 1;
            RegisterFile.PS.N = ((0x80 & Operand)>>7);
            RegisterFile.PS.Z = (Operand == 0);
            WriteByte(clk, mem, LoadAddress, Operand);
        }
        break;
        case ASL_ABS:
        {
            ADDRESS LoadAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            WriteByte(clk, mem, LoadAddress, Operand);
            RegisterFile.PS.C = ((0x80 & Operand) >> 7);
            Operand = Operand << 1;
            RegisterFile.PS.N = ((0x80 & Operand)>>7);
            RegisterFile.PS.Z = (Operand == 0);
            WriteByte(clk, mem, LoadAddress, Operand);
        }
        break;
        case ASL_ABS_X:
        {
            ADDRESS LoadAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, true);
            BYTE Operand = ReadByte(clk, mem, LoadAddress);
            WriteByte(clk, mem, LoadAddress, Operand);
            RegisterFile.PS.C = ((0x80 & Operand) >> 7);
            Operand = Operand << 1;
            RegisterFile.PS.N = ((0x80 & Operand)>>7);
            RegisterFile.PS.Z = (Operand == 0);
            WriteByte(clk, mem, LoadAddress, Operand);
        }
        break;
        case BCC:
        {
            BYTE Offset = FetchByte(clk, mem);
            if(RegisterFile.PS.C == false)
            {
                ADDRESS TargetAddress = AMGetRelativeAddress(clk, mem, Offset);
                RegisterFile.PC = TargetAddress;
                clk.IncrementTick(1);
            }
        }
        break;
        case BCS:
        {
            BYTE Offset = FetchByte(clk, mem);
            if(RegisterFile.PS.C == true)
            {
                ADDRESS TargetAddress = AMGetRelativeAddress(clk, mem, Offset);
                RegisterFile.PC = TargetAddress;
                clk.IncrementTick(1);
            }
        }
        break;
        case BEQ:
        {
            BYTE Offset = FetchByte(clk, mem);
            if(RegisterFile.PS.Z == true)
            {
                ADDRESS TargetAddress = AMGetRelativeAddress(clk, mem, Offset);
                RegisterFile.PC = TargetAddress;
                clk.IncrementTick(1);
            }
        }
        break;
        case BIT_ZP:
        {
            ADDRESS LoadAddress = AMGetZeroPageAddress(clk, mem);
            BYTE OperandB = ReadByte(clk, mem, LoadAddress);
            OP_RESULT result = BitTest(RegisterFile.A, OperandB);
            RegisterFile.PS = result.flags;
        }
        break;
        case BIT_ABS:
        {
            ADDRESS LoadAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE OperandB = ReadByte(clk, mem, LoadAddress);
            OP_RESULT result = BitTest(RegisterFile.A, OperandB);
            RegisterFile.PS = result.flags;
        }
        break;
        case BMI:
        {
            BYTE Offset = FetchByte(clk, mem);
            if(RegisterFile.PS.N == true)
            {
                ADDRESS TargetAddress = AMGetRelativeAddress(clk, mem, Offset);
                RegisterFile.PC = TargetAddress;
                clk.IncrementTick(1);
            }
        }
        break;
        case BNE:
        {
            BYTE Offset = FetchByte(clk, mem);
            if(RegisterFile.PS.Z == false)
            {
                ADDRESS TargetAddress = AMGetRelativeAddress(clk, mem, Offset);
                RegisterFile.PC = TargetAddress;
                clk.IncrementTick(1);
            }
        }
        break;
        case BPL:
        {
            BYTE Offset = FetchByte(clk, mem);
            if(RegisterFile.PS.N == false)
            {
                ADDRESS TargetAddress = AMGetRelativeAddress(clk, mem, Offset);
                RegisterFile.PC = TargetAddress;
                clk.IncrementTick(1);
            }
        }
        break;
        case BRK:
        {
            FetchByte(clk, mem);
            PushAddressToStack(clk, mem, RegisterFile.PC);
            RegisterFile.PS.B = true;
            PushByteToStack(clk, mem, RegisterFile.PS.ToByte());
            RegisterFile.PC = ReadAddress(clk, mem, 0xFFFE);
        }
        break;
        case BVC:
        {
            BYTE Offset = FetchByte(clk, mem);
            if(RegisterFile.PS.V == false)
            {
                ADDRESS TargetAddress = AMGetRelativeAddress(clk, mem, Offset);
                RegisterFile.PC = TargetAddress;
                clk.IncrementTick(1);
            }
        }
        break;
        case BVS:
        {
            BYTE Offset = FetchByte(clk, mem);
            if(RegisterFile.PS.V == true)
            {
                ADDRESS TargetAddress = AMGetRelativeAddress(clk, mem, Offset);
                RegisterFile.PC = TargetAddress;
                clk.IncrementTick(1);
            }
        }
        break;
        case CLC:
        {
            FetchByte(clk, mem);
            RegisterFile.PS.C = 0;
        }
        break;
        case CLD:
        {
            FetchByte(clk, mem);
            RegisterFile.PS.D = 0;
        }
        break;
        case CLI:
        {
            FetchByte(clk, mem);
            RegisterFile.PS.I = 0;
        }
        break;
        case CLV:
        {
            FetchByte(clk, mem);
            RegisterFile.PS.V = 0;
        }
        break;
        case CMP_IM:
        {
            BYTE OpB = FetchByte(clk, mem);
            OP_RESULT CmpRes = Compare(RegisterFile.A, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CMP_ZP:
        {
            ADDRESS ZPAddress = AMGetZeroPageAddress(clk, mem);
            BYTE OpB = ReadByte(clk, mem, ZPAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.A, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CMP_ZP_X:
        {
            ADDRESS ZPXAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE OpB = ReadByte(clk, mem, ZPXAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.A, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CMP_ABS:
        {
            ADDRESS AbsAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE OpB = ReadByte(clk, mem, AbsAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.A, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CMP_ABS_X:
        {
            ADDRESS IndAbsAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, false);
            BYTE OpB = ReadByte(clk, mem, IndAbsAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.A, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CMP_ABS_Y:
        {
            ADDRESS IndAbsAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.Y, false);
            BYTE OpB = ReadByte(clk, mem, IndAbsAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.A, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CMP_IND_X:
        {
            ADDRESS IndexedIndirectAddress = AMGetIndexedIndirectAddress(clk, mem);
            BYTE OpB = ReadByte(clk, mem, IndexedIndirectAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.A, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CMP_IND_Y:
        {
            ADDRESS IndirectIndexedAddess = AMGetIndirectIndexedAddress(clk, mem, false);
            BYTE OpB = ReadByte(clk, mem, IndirectIndexedAddess);
            OP_RESULT CmpRes = Compare(RegisterFile.A, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CPX_IM:
        {
            BYTE OpB = FetchByte(clk, mem);
            OP_RESULT CmpRes = Compare(RegisterFile.X, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CPX_ZP:
        {
            ADDRESS ZPAddress = AMGetZeroPageAddress(clk, mem);
            BYTE OpB = ReadByte(clk, mem, ZPAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.X, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CPX_ABS:
        {
            ADDRESS AbsAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE OpB = ReadByte(clk, mem, AbsAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.X, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CPY_IM:
        {
            BYTE OpB = FetchByte(clk, mem);
            OP_RESULT CmpRes = Compare(RegisterFile.Y, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CPY_ZP:
        {
            ADDRESS ZPAddress = AMGetZeroPageAddress(clk, mem);
            BYTE OpB = ReadByte(clk, mem, ZPAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.Y, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case CPY_ABS:
        {
            ADDRESS AbsAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE OpB = ReadByte(clk, mem, AbsAddress);
            OP_RESULT CmpRes = Compare(RegisterFile.Y, OpB);
            RegisterFile.PS = CmpRes.flags;
        }
        break;
        case DEC_ZP:
        {
            ADDRESS ZeroPageAddress = AMGetZeroPageAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            Operand--;
            if(Operand == 0) RegisterFile.PS.Z = true;
            if((Operand & 0b10000000) != 0) RegisterFile.PS.N = true;
            WriteByte(clk, mem, ZeroPageAddress, Operand);
        }
        break;
        case DEC_ZP_X:
        {
            ADDRESS ZeroPageAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            Operand--;
            if(Operand == 0) RegisterFile.PS.Z = true;
            if((Operand & 0b10000000) != 0) RegisterFile.PS.N = true;
            WriteByte(clk, mem, ZeroPageAddress, Operand);
        }
        break;
        case DEC_ABS:
        {
            ADDRESS ZeroPageAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            Operand--;
            if(Operand == 0) RegisterFile.PS.Z = true;
            if((Operand & 0b10000000) != 0) RegisterFile.PS.N = true;
            WriteByte(clk, mem, ZeroPageAddress, Operand);
        }
        break;
        case DEC_ABS_X:
        {
            ADDRESS ZeroPageAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, true);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            Operand--;
            if(Operand == 0) RegisterFile.PS.Z = true;
            if((Operand & 0b10000000) != 0) RegisterFile.PS.N = true;
            WriteByte(clk, mem, ZeroPageAddress, Operand);
        }
        break;
        case DEX:
        {
            FetchByte(clk, mem);
            --RegisterFile.X;
            if (RegisterFile.X == 0) RegisterFile.PS.Z = true;
            if ((RegisterFile.X & 0b10000000) != 0) RegisterFile.PS.N = true;
        }
        break;
        case DEY:
        {
            FetchByte(clk, mem);
            --RegisterFile.Y;
            if (RegisterFile.Y == 0) RegisterFile.PS.Z = true;
            if ((RegisterFile.Y & 0b10000000) != 0) RegisterFile.PS.N = true;
        }
        break;
        case EOR_IM:
        {
            BYTE Immediate = FetchByte(clk, mem);
            OP_RESULT Result = ExclusiveOR(RegisterFile.A, Immediate);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case EOR_ZP:
        {
            ADDRESS ZeroPageAddres = AMGetZeroPageAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddres);
            OP_RESULT Result = ExclusiveOR(RegisterFile.A, Operand);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case EOR_ZP_X:
        {
            ADDRESS ZeroPageAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            OP_RESULT Result = ExclusiveOR(RegisterFile.A, Operand);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case EOR_ABS:
        {
            ADDRESS AbsoluteAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, AbsoluteAddress);
            OP_RESULT Result = ExclusiveOR(RegisterFile.A, Operand);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case EOR_ABS_X:
        {
            ADDRESS AbsoluteIndexedAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, false);
            BYTE Operand = ReadByte(clk, mem, AbsoluteIndexedAddress);
            OP_RESULT Result = ExclusiveOR(RegisterFile.A, Operand);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case EOR_ABS_Y:
        {
            ADDRESS AbsoluteIndexedAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.Y, false);
            BYTE Operand = ReadByte(clk, mem, AbsoluteIndexedAddress);
            OP_RESULT Result = ExclusiveOR(RegisterFile.A, Operand);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case EOR_IND_X:
        {
            ADDRESS IndexedIndirectAddress = AMGetIndexedIndirectAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, IndexedIndirectAddress);
            OP_RESULT Result = ExclusiveOR(RegisterFile.A, Operand);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case EOR_IND_Y:
        {
            ADDRESS IndirectIndexedAddress = AMGetIndirectIndexedAddress(clk, mem, false);
            BYTE Operand = ReadByte(clk, mem, IndirectIndexedAddress);
            OP_RESULT Result = ExclusiveOR(RegisterFile.A, Operand);
            RegisterFile.PS = Result.flags;
            RegisterFile.A = Result.number;
        }
        break;
        case INC_ZP:
        {
            ADDRESS ZeroPageAddress = AMGetZeroPageAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            Operand++;
            if(Operand == 0) RegisterFile.PS.Z = true;
            if((Operand & 0b10000000) != 0) RegisterFile.PS.N = true;
            WriteByte(clk, mem, ZeroPageAddress, Operand);
        }
        break;
        case INC_ZP_X:
        {
            ADDRESS ZeroPageAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            Operand++;
            if(Operand == 0) RegisterFile.PS.Z = true;
            if((Operand & 0b10000000) != 0) RegisterFile.PS.N = true;
            WriteByte(clk, mem, ZeroPageAddress, Operand);
        }
        break;
        case INC_ABS:
        {
            ADDRESS ZeroPageAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            Operand++;
            if(Operand == 0) RegisterFile.PS.Z = true;
            if((Operand & 0b10000000) != 0) RegisterFile.PS.N = true;
            WriteByte(clk, mem, ZeroPageAddress, Operand);
        }
        break;
        case INC_ABS_X:
        {
            ADDRESS ZeroPageAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, true);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            Operand++;
            if(Operand == 0) RegisterFile.PS.Z = true;
            if((Operand & 0b10000000) != 0) RegisterFile.PS.N = true;
            WriteByte(clk, mem, ZeroPageAddress, Operand);
        }
        break;
        case INX:
        {
            FetchByte(clk, mem);
            ++RegisterFile.X;
            if (RegisterFile.X == 0) RegisterFile.PS.Z = true;
            if ((RegisterFile.X & 0b10000000) != 0) RegisterFile.PS.N = true;
        }
        break;
        case INY:
        {
            FetchByte(clk, mem);
            ++RegisterFile.Y;
            if (RegisterFile.Y == 0) RegisterFile.PS.Z = true;
            if ((RegisterFile.Y & 0b10000000) != 0) RegisterFile.PS.N = true;
        }
        break;
        case JMP_ABS:
        {
            ADDRESS TargetAddress = FetchAddress(clk, mem);
            RegisterFile.PC = TargetAddress;
        }
        break;
        case JMP_IND:
        {
            ADDRESS TargetAddress = AMGetIndirectAddress(clk, mem);
            RegisterFile.PC = TargetAddress;
        }
        break;
        case JSR:
        {
            /* The JSR instructions pushes to the stack the current PC-1. */
            ADDRESS SubroutineAddress = FetchAddress(clk, mem);
            PushAddressToStack(clk, mem, CPU::RegisterFile.PC - 1);
            clk.IncrementTick(1);
            CPU::RegisterFile.PC = SubroutineAddress;
        }
        break;
        case LDA_IM:
        {
            BYTE Value = FetchByte(clk, mem);
            UpdateA(Value);
        }
        break;
        case LDA_ZP:
        {
            ADDRESS ZeroPageAddress = AMGetZeroPageAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            UpdateA(Operand);
        }
        break;
        case LDA_ZP_X:
        {
            ADDRESS ZeroPageIndexedAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE Operand = ReadByte(clk, mem, ZeroPageIndexedAddress);
            UpdateA(Operand);
        }
        break;
        case LDA_ABS:
        {
            ADDRESS AbsoluteAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, AbsoluteAddress);
            UpdateA(Operand);
        }
        break;
        case LDA_ABS_X:
        {

            ADDRESS AbsoluteAddess = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, false);
            BYTE Operand = ReadByte(clk, mem, AbsoluteAddess);
            UpdateA(Operand);
        }
        break;
        case LDA_ABS_Y:
        {
            ADDRESS AbsoluteAddess = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.Y, false);
            BYTE Operand = ReadByte(clk, mem, AbsoluteAddess);
            UpdateA(Operand);
        }
        break;
        case LDA_IND_X:
        {
            ADDRESS IndexedIndirectAddress = AMGetIndexedIndirectAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, IndexedIndirectAddress);
            UpdateA(Operand);
        }
        break;
        case LDA_IND_Y:
        {
            ADDRESS IndirectIndexedAdress = AMGetIndirectIndexedAddress(clk, mem, false);
            BYTE Operand = ReadByte(clk, mem, IndirectIndexedAdress);
            UpdateA(Operand);
        }
        break;
        case LDX_IM:
        {
            /** Fetch immediate. */
            BYTE Value = FetchByte(clk, mem);
            /** Update with the new value. */
            UpdateX(Value);
        }
        break;
        case LDX_ZP:
        {
            ADDRESS ZeroPageAddress = AMGetZeroPageAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            UpdateX(Operand);
        }
        break;
        case LDX_ZP_Y:
        {
            ADDRESS ZeroPageIndexedAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.Y);
            BYTE Operand = ReadByte(clk, mem, ZeroPageIndexedAddress);
            UpdateX(Operand);
        }
        break;
        case LDX_ABS:
        {
            ADDRESS AbsoluteAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, AbsoluteAddress);
            UpdateX(Operand);
        }
        break;
        case LDX_ABS_Y:
        {
            ADDRESS AbsoluteIndexedAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.Y, false);
            BYTE Operand = ReadByte(clk, mem, AbsoluteIndexedAddress);
            UpdateX(Operand);
        }
        break;
        case LDY_IM:
        {
            /** Fetch immediate. */
            BYTE Value = FetchByte(clk, mem);
            /** Update with the new value. */
            UpdateY(Value);
        }
        break;
        case LDY_ZP:
        {
            /** Fetch data from Zero Page. */
            ADDRESS ZeroPageAddress = AMGetZeroPageAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            UpdateY(Operand);
        }
        break;
        case LDY_ZP_X:
        {
            ADDRESS ZeroPageIndexedAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE Operand = ReadByte(clk, mem, ZeroPageIndexedAddress);
            UpdateY(Operand);
        }
        break;
        case LDY_ABS:
        {
            ADDRESS AbsoluteAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, AbsoluteAddress);
            UpdateY(Operand);
        }
        break;
        case LDY_ABS_X:
        {
            ADDRESS AbsoluteIndexedAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, false);
            BYTE Operand = ReadByte(clk, mem, AbsoluteIndexedAddress);
            UpdateY(Operand);
        }
        break;
        case LSR_A:
        {
            /**
             * As in NOP instruction, when the Accumulator is accessed
             * the processor reads a dummy byte from the memory. This 
             * is also called accumulator addressing mode.
            */
            FetchByte(clk, mem);
            OP_RESULT Result = RightShift(RegisterFile.A);
            RegisterFile.A = Result.number;
            RegisterFile.PS = Result.flags;
        }
        break;
        case LSR_ZP:
        {
            ADDRESS ZeroPageAddress = AMGetZeroPageAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            OP_RESULT Result = RightShift(Operand);
            RegisterFile.PS = Result.flags;
            WriteByte(clk, mem, ZeroPageAddress, Result.number);            
        }
        break;
        case LSR_ZP_X:
        {
            ADDRESS ZeroPageAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            BYTE Operand = ReadByte(clk, mem, ZeroPageAddress);
            WriteByte(clk, mem, ZeroPageAddress, Operand);
            OP_RESULT Result = RightShift(Operand);
            RegisterFile.PS = Result.flags;
            WriteByte(clk, mem, ZeroPageAddress, Result.number);  
        }
        break;
        case LSR_ABS:
        {
            ADDRESS AbsoluteAddress = AMGetAbsoluteAddress(clk, mem);
            BYTE Operand = ReadByte(clk, mem, AbsoluteAddress);
            WriteByte(clk, mem, AbsoluteAddress, Operand);
            OP_RESULT Result = RightShift(Operand);
            RegisterFile.PS = Result.flags;
            WriteByte(clk, mem, AbsoluteAddress, Result.number);
        }
        break;
        case LSR_ABS_X:
        {
            ADDRESS AbsoluteAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, true);
            BYTE Operand = ReadByte(clk, mem, AbsoluteAddress);
            WriteByte(clk, mem, AbsoluteAddress, Operand);
            OP_RESULT Result = RightShift(Operand);
            RegisterFile.PS = Result.flags;
            WriteByte(clk, mem, AbsoluteAddress, Result.number);
        }
        break;
        case NOP:
        {
            /** 
             * The NOP instruction is just like any other load instruction, 
             * except it does not store the result anywhere nor affects the flags.
             * 
             * Source: http://www.atarihq.com/danb/files/64doc.txt
             * 
             * The processor normally reads the byte following the instruction, 
             * just like any other instruction, but ignores it. In the emulator, 
             * we can just increase the PC and the resulting expression has the 
             * same effect.
             */
            FetchByte(clk, mem);
        }
        break;
        case PHA:
        {
            FetchByte(clk, mem);
            PushByteToStack(clk, mem, RegisterFile.A);
        }
        break;
        case PHP:
        {
            FetchByte(clk, mem);
            PushByteToStack(clk, mem, RegisterFile.PS.ToByte());
        }
        break;
        case PLA:
        {
            FetchByte(clk, mem);
            BYTE Value = PullByteFromStack(clk, mem);
            UpdateA(Value);
        }
        break;
        case PLP:
        {
            FetchByte(clk, mem);
            BYTE Value = PullByteFromStack(clk, mem);
            RegisterFile.PS.FromByte(Value);
        }
        break;
        case RTI:
        {
            FetchByte(clk, mem);
            BYTE SavedFlags = PullByteFromStack(clk, mem);
            RegisterFile.PS.FromByte(SavedFlags);
            ++RegisterFile.SP;
            ADDRESS ReturnAddress = PullAddressFromStack(clk, mem);
            RegisterFile.PC = ReturnAddress;
        }
        break;
        case RTS:
        {
            FetchByte(clk, mem);
            clk.IncrementTick(1);
            ++RegisterFile.SP;
            ADDRESS ReturnAddress = PullAddressFromStack(clk, mem);
            clk.IncrementTick(1);
            RegisterFile.PC = ReturnAddress + 1;
        }
        break;
        case SEC:
        {
            FetchByte(clk, mem);
            RegisterFile.PS.C = 1;
        }
        break;
        case SED:
        {
            FetchByte(clk, mem);
            RegisterFile.PS.D = 1;
        }
        break;
        case SEI:
        {
            FetchByte(clk, mem);
            RegisterFile.PS.I = 1;
        }
        break;
        case STA_ZP:
        {
            ADDRESS StoreAddress = AMGetZeroPageAddress(clk, mem);
            WriteByte(clk, mem, StoreAddress, RegisterFile.A);
        }
        break;
        case STA_ZP_X:
        {
            ADDRESS StoreAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            WriteByte(clk, mem, StoreAddress, RegisterFile.A);
        }
        break;
        case STA_ABS:
        {
            ADDRESS StoreAddress = AMGetAbsoluteAddress(clk, mem);
            WriteByte(clk, mem, StoreAddress, RegisterFile.A);
        }
        break;
        case STA_ABS_X:
        {
            ADDRESS StoreAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.X, true);
            WriteByte(clk, mem, StoreAddress, RegisterFile.A);
        }
        break;
        case STA_ABS_Y:
        {
            ADDRESS StoreAddress = AMGetAbsoluteIndexedAddress(clk, mem, RegisterFile.Y, true);
            WriteByte(clk, mem, StoreAddress, RegisterFile.A);
        }
        break;
        case STA_IND_X:
        {
            ADDRESS StoreAddress = AMGetIndexedIndirectAddress(clk, mem);
            WriteByte(clk, mem, StoreAddress, RegisterFile.A);
        }
        break;
        case STA_IND_Y:
        {
            ADDRESS StoreAddress = AMGetIndirectIndexedAddress(clk, mem, true);
            WriteByte(clk, mem, StoreAddress, RegisterFile.A);
        }
        break;
        case STX_ZP:
        {
            ADDRESS StoreAddress = AMGetZeroPageAddress(clk, mem);
            WriteByte(clk, mem, StoreAddress, RegisterFile.X);
        }
        break;
        case STX_ZP_Y:
        {
            ADDRESS StoreAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.Y);
            WriteByte(clk, mem, StoreAddress, RegisterFile.X);
        }
        break;
        case STX_ABS:
        {
            ADDRESS StoreAddress = AMGetAbsoluteAddress(clk, mem);
            WriteByte(clk, mem, StoreAddress, RegisterFile.X);
        }
        break;
        case STY_ZP:
        {
            ADDRESS StoreAddress = AMGetZeroPageAddress(clk, mem);
            WriteByte(clk, mem, StoreAddress, RegisterFile.Y);
        }
        break;
        case STY_ZP_X:
        {
            ADDRESS StoreAddress = AMGetZeroPageIndexedAddress(clk, mem, RegisterFile.X);
            WriteByte(clk, mem, StoreAddress, RegisterFile.Y);
        }
        break;
        case STY_ABS:
        {
            ADDRESS StoreAddress = AMGetAbsoluteAddress(clk, mem);
            WriteByte(clk, mem, StoreAddress, RegisterFile.Y);
        }
        break;
        case TAX:
        {
            /** Implied addressing */
            FetchByte(clk, mem);
            UpdateX(RegisterFile.A);
        }
        break;
        case TAY:
        {
            /** Implied addressing */
            FetchByte(clk, mem);
            UpdateY(RegisterFile.A);
        }
        break;
        case TSX:
        {
            /** Implied addressing */
            FetchByte(clk, mem);
            UpdateX(RegisterFile.SP);
        }
        break;
        case TXA:
        {
            /** Implied addressing */
            FetchByte(clk, mem);
            UpdateA(RegisterFile.X);
        }
        break;
        case TXS:
        {
            /** Implied addressing */
            FetchByte(clk, mem);
            RegisterFile.SP = RegisterFile.X;
        }
        break;
        case TYA:
        {
            /** Implied addressing */
            FetchByte(clk, mem);
            UpdateA(RegisterFile.Y);
        }
        break;
        default:
            break;
        }
    }
}
