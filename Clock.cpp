#include "Clock.h"

Clock::Clock()
{
    ticks = 0;
    ticks_breakpoint = 0;
    instruction_number = 0;
    instruction_breakpoint = 0;
}
bool Clock::Proceed()
{
    if (break_instructions)
    {
        return (instruction_number < instruction_breakpoint);
    }
    else
        return true;
}
void Clock::IncrementTick(uint64_t q)
{
    ticks += q;
}
void Clock::IncrementInstruction()
{
    ++instruction_number;
}
/*By setting the step we specify how many instructions we want to execute.*/
void Clock::SetInstructionStep(uint64_t step)
{
    break_instructions = true;
    instruction_breakpoint = instruction_number + step;
}
void Clock::StartCounting()
{
    this->elapsed_start = ticks;
}
uint64_t Clock::GetElapsedTime()
{
    this->elapsed_end = ticks;
    return (this->elapsed_end - this->elapsed_start);
}