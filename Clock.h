#ifndef CLOCK_H
#define CLOCK_H

#include <stdint.h>

class Clock
{
public:
    Clock();
    bool Proceed();
    void IncrementTick(uint64_t q);
    void IncrementInstruction();
    /*By setting the step we specify how many instructions we want to execute.*/
    void SetInstructionStep(uint64_t step);
    void StartCounting();
    uint64_t GetElapsedTime();
private:
    uint64_t elapsed_start;
    uint64_t elapsed_end;
    uint64_t ticks;
    uint64_t ticks_breakpoint;
    bool break_ticks;
    uint64_t instruction_number;
    uint64_t instruction_breakpoint;
    bool break_instructions;
};

#endif