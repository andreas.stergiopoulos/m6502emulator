#ifndef CPU_H
#define CPU_H

#include "./gtest/gtest.h"

#ifndef DEFS_H
#include "defs.h"
#endif
#ifndef MEMORY_H
#include "Memory.h"
#endif
#ifndef CLOCK_H
#include "Clock.h"
#endif
#ifndef OPCODES_H
#include "Opcodes.h"
#endif

class CPU
{
public:
    void Reset(Memory &mem);
    void Execute(Clock &clk, Memory &mem);

    static constexpr ADDRESS RESET_VECTOR = 0xFFFC;

    struct STATUS_FLAGS
    {
        BYTE C : 1; //Carry Flag
        BYTE Z : 1; //Zero Flag
        BYTE I : 1; //Interrupts Disable Flag
        BYTE D : 1; //Decimal Mode Flag
        BYTE B : 1; //Break Command FLag
        BYTE V : 1; //Overflow flag
        BYTE N : 1; //Negative Flag

        bool inline operator==(const STATUS_FLAGS &a) const
        {
            return ((this->C == a.C) &&
                    (this->Z == a.Z) &&
                    (this->I == a.I) &&
                    (this->D == a.D) &&
                    (this->B == a.B) &&
                    (this->V == a.V) &&
                    (this->N == a.N));
        }

        BYTE ToByte()
        {
            BYTE ByteValue = 0;
            ByteValue |= C;
            ByteValue |= Z << 1;
            ByteValue |= I << 2;
            ByteValue |= D << 3;
            ByteValue |= B << 4;
            ByteValue |= 1 << 5; //Unused bit.
            ByteValue |= V << 6;
            ByteValue |= N << 7;
            return ByteValue;
        }

        void FromByte(BYTE ByteValue)
        {
            C = ByteValue & 0x1;
            Z = (ByteValue >> 1) & 0x1;
            I = (ByteValue >> 2) & 0x1;
            D = (ByteValue >> 3) & 0x1;
            B = (ByteValue >> 4) & 0x1;
            //Unused bit.
            V = (ByteValue >> 6) & 0x1;
            N = (ByteValue >> 7) & 0x1;
        }
    };

    /**
     * This struct is used to group all the registers in order to ease debugging.
    */
    struct REGISTER_FILE
    {
        /**
         * Program Counter. The program counter points to the address 
         * where the next instruction (or piece of instruction) is located.
         * 
         * The PC is incremented AFTER fetching the content.
        */
        ADDRESS PC;

        /**
         * The stack on the 6502 is situated in the address space 0x100-0x1FF 
         * and it grows top-down. That means that when we push to the stack, 
         * we decrement the stack pointer and when we pull (that's the term for
         * the 6502) we increment the stack pointer.
         * 
         * The Stack Pointer is a single byte that is always mapped in the region 
         * 0x1--. Thus, the stack is not transferrable and it is not possible to 
         * have more than one stacks at a time.
         * 
         * The size is a fixed 256 bytes and the processor does not perform any 
         * checks for stack overflow. Thus, a stack overflow can easily crash the 
         * program.
        */
        BYTE SP;

        /**
         * The three registers of the 6502 are the accumulator A and the 
         * index registers X and Y.
        */
        BYTE A;
        BYTE X;
        BYTE Y;

        STATUS_FLAGS PS;

        bool inline operator==(const REGISTER_FILE &a) const
        {
            /** The Program Counter has been ignored intentionally. */
            return ((this->SP == a.SP) &&
                    (this->A == a.A) &&
                    (this->X == a.X) &&
                    (this->Y == a.Y) &&
                    (this->PS == a.PS));
        }
    } RegisterFile;

    /* OPeration RESULT struct. Used to hold the numerical and flags results
    that occur after an operation. */
    struct OP_RESULT
    {
        BYTE number;
        STATUS_FLAGS flags;
    };

    /*Fetch a byte from memory, incrementing the program counter.*/
    BYTE FetchByte(Clock &clk, Memory &mem);

    /*Fetch a word from memory using the Program Counter. 6502 is little endian.*/
    ADDRESS FetchAddress(Clock &clk, Memory &mem);

    /*Write a word in a location in memory.*/
    void WriteAddress(Clock &clk, Memory &mem, ADDRESS address, ADDRESS word);

    /*Write a word in a location in memory.*/
    void WriteByte(Clock &clk, Memory &mem, ADDRESS address, BYTE byte);

    /*Read a byte from memory without incrementing the program counter.*/
    BYTE ReadByte(Clock &clk, Memory &mem, ADDRESS address);
    /*Fetch a word from memory without incrementing the Program Counter. 6502 is little endian.*/
    ADDRESS ReadAddress(Clock &clk, Memory &mem, ADDRESS address);

    /*Push an address to the stack.*/
    void PushAddressToStack(Clock &clk, Memory &mem, ADDRESS word);

    /*Pull an address from the stack.*/
    ADDRESS PullAddressFromStack(Clock &clk, Memory &mem);

    /*Push a byte to the stack.*/
    void PushByteToStack(Clock &clk, Memory &mem, BYTE byte);

    /*Pull a byte from the stack.*/
    BYTE PullByteFromStack(Clock &clk, Memory &mem);

    /** Update the value on register X. */
    void UpdateX(BYTE value);

    /** Update the value on register Y. */
    void UpdateY(BYTE value);

    /** Update the value on register A. */
    void UpdateA(BYTE value);

    /** Update the Program Status register after executing an LD_ opcode. */
    void LDUpdatePS(BYTE value);

    /** Return true if we cross the boundaries of a page. */
    bool PageCrossDetection(ADDRESS Address, BYTE Offset);

    /**
     * Indirect Indexed vs Indexed Indirect addressing modes:
     * 
     * In both modes, we reach for an address that is addressed in the Zero Page.
     * 
     * In Indexed Indirect, we add the offset to the Zero Page address and do a wrap-around 
     * if the resulting address crosses the page boundaries. In programming terms:
     * FinalAddress = (ZeroPageAddress + Offset) & 0xFF
     * 
     * In Indirect Indexed, we reach to get the address from the Zero Page again, and then we 
     * add the specified offset. If the addition produces a carry, we must use one additional 
     * cycle in order to propagate the carry.
    */

    /**
     * Indexed Indirect Addressing Mode. USED WITH X.
     * 
     * (Take in byte immediate, add X, wrap around 0xFF, calculate new address within zero page.)
     * 
     * 1] Fetch the immediate needed, add to X and wrap around the Zero Page.
     * The fetching and addition to X happes at the same cycle.
     * 
     * 2] Read the final address from the location inside the Zero Page.
     * 
     * This mode is only used with the X register. Consider a situation where the instruction 
     * is LDA ($20,X), X contains $04, and memory at $24 contains 0024: 74 20, First, X is added 
     * to $20 to get $24. The target address will be fetched from $24 resulting in a target 
     * address of $2074. Register A will be loaded with the contents of memory at $2074.
     * 
     * If X + the immediate byte will wrap around to a zero-page address. So you could code that 
     * like targetAddress = (X + opcode[1]) & 0xFF .
     * 
     * Indexed Indirect instructions are 2 bytes - the second byte is the zero-page address - $20 
     * in the example. Obviously the fetched address has to be stored in the zero page.
     * 
     * Source: http://www.emulator101.com/6502-addressing-modes.html
    */
    BYTE AMIndexedIndirect(Clock &clk, Memory &mem);

    /**
     * Indirect Indexed Addressing Mode. USED WITH Y.
     * 
     * (Take in byte that is Zero Page address, read 16-bit address from that Zero Page address,
     * add offset and do not wrap around. If address addition produces carry, take one extra cycle.)
     * 
     * 1] Fetch the immediate that gives the Zero Page address.
     * 
     * 2] Read the address from the Zero Page
     * 
     * 3] Add Y and check for page crossing.
     *      -If we cross the page boundary, use one extra cycle. This happens because
     *       we must use the addition circuitry in order to propagate the carry.
     * 
     * This mode is only used with the Y register. It differs in the order that Y is applied to the 
     * indirectly fetched address. An example instruction that uses indirect index addressing is 
     * LDA ($86),Y . To calculate the target address, the CPU will first fetch the address stored at 
     * zero page location $86. That address will be added to register Y to get the final target address. 
     * For LDA ($86),Y, if the address stored at $86 is $4028 (memory is 0086: 28 40, remember little 
     * endian) and register Y contains $10, then the final target address would be $4038. Register A 
     * will be loaded with the contents of memory at $4038.
     * 
     * Indirect Indexed instructions are 2 bytes - the second byte is the zero-page address - $86 in 
     * the example. (So the fetched address has to be stored in the zero page.)
     * 
     * While indexed indirect addressing will only generate a zero-page address, this mode's target 
     * address is not wrapped - it can be anywhere in the 16-bit address space.
     * 
     * Source: http://www.emulator101.com/6502-addressing-modes.html
    */
    BYTE AMIndirectIndexed(Clock &clk, Memory &mem);

    /** Addressing Mode: Load From Absolute. */
    ADDRESS AMGetAbsoluteAddress(Clock &clk, Memory &mem);

    /** 
     * Absolute Indexed Adressing Mode.
     * 
     * Add offset to absolute address loaded using PC and use extra tick if 
     * page is crossed. 
     * 
     * If PenaltyCycle is true, we do not check for page crossing and add one extra 
     * cycle either way. This is used for the store operations.
     *
     * Explanation: https://retrocomputing.stackexchange.com/questions/15621/why-dont-all-absolute-x-instructions-take-an-extra-cycle-to-cross-page-boundari
    */
    ADDRESS AMGetAbsoluteIndexedAddress(Clock &clk, Memory &mem, BYTE offset, bool PenaltyCycle);

    /**
     * Zero Page Addressing Mode.
     * 
     * Load value from address in Zero Page. Target address is fetched using the PC.
    */
    ADDRESS AMGetZeroPageAddress(Clock &clk, Memory &mem);

    /**
     * Zero Page Indexed Addressing Mode.
     * 
     * Fetch Zero Page address using the PC and then add the offset. The target address
     * is always in the Zero Page.
    */
    ADDRESS AMGetZeroPageIndexedAddress(Clock &clk, Memory &mem, BYTE offset);

    /**
     * Indirect Indexed vs Indexed Indirect addressing modes:
     * 
     * In both modes, we reach for an address that is addressed in the Zero Page.
     * 
     * In Indexed Indirect, we add the offset to the Zero Page address and do a wrap-around 
     * if the resulting address crosses the page boundaries. In programming terms:
     * FinalAddress = (ZeroPageAddress + Offset) & 0xFF
     * 
     * In Indirect Indexed, we reach to get the address from the Zero Page again, and then we 
     * add the specified offset. If the addition produces a carry, we must use one additional 
     * cycle in order to propagate the carry.
    */

    /**
     * Indexed Indirect Addressing Mode.
     * 
     * (Take in byte immediate, add X, wrap around 0xFF, calculate new address within zero page.)
     * 
     * 1] Fetch the immediate needed, add to X and wrap around the Zero Page.
     * The fetching and addition to X happes at the same cycle.
     * 
     * 2] Read the final address from the location inside the Zero Page.
     * 
     * This mode is only used with the X register. Consider a situation where the instruction 
     * is LDA ($20,X), X contains $04, and memory at $24 contains 0024: 74 20, First, X is added 
     * to $20 to get $24. The target address will be fetched from $24 resulting in a target 
     * address of $2074. Register A will be loaded with the contents of memory at $2074.
     * 
     * If X + the immediate byte will wrap around to a zero-page address. So you could code that 
     * like targetAddress = (X + opcode[1]) & 0xFF .
     * 
     * Indexed Indirect instructions are 2 bytes - the second byte is the zero-page address - $20 
     * in the example. Obviously the fetched address has to be stored in the zero page.
     * 
     * Source: http://www.emulator101.com/6502-addressing-modes.html
    */
    ADDRESS AMGetIndexedIndirectAddress(Clock &clk, Memory &mem);

    /**
     * Indirect Indexed Addressing Mode.
     * 
     * (Take in byte that is Zero Page address, read 16-bit address from that Zero Page address,
     * add offset and do not wrap around. If address addition produces carry, take one extra cycle.)
     * 
     * 1] Fetch the immediate that gives the Zero Page address.
     * 
     * 2] Read the address from the Zero Page
     * 
     * 3] Add Y and check for page crossing.
     *      -If we cross the page boundary, use one extra cycle. This happens because
     *       we must use the addition circuitry in order to propagate the carry.
     * 
     * This mode is only used with the Y register. It differs in the order that Y is applied to the 
     * indirectly fetched address. An example instruction that uses indirect index addressing is 
     * LDA ($86),Y . To calculate the target address, the CPU will first fetch the address stored at 
     * zero page location $86. That address will be added to register Y to get the final target address. 
     * For LDA ($86),Y, if the address stored at $86 is $4028 (memory is 0086: 28 40, remember little 
     * endian) and register Y contains $10, then the final target address would be $4038. Register A 
     * will be loaded with the contents of memory at $4038.
     * 
     * Indirect Indexed instructions are 2 bytes - the second byte is the zero-page address - $86 in 
     * the example. (So the fetched address has to be stored in the zero page.)
     * 
     * While indexed indirect addressing will only generate a zero-page address, this mode's target 
     * address is not wrapped - it can be anywhere in the 16-bit address space.
     * 
     * Source: http://www.emulator101.com/6502-addressing-modes.html
     *
     * PenaltyCycle: See Absolute Indexed.
    */
    ADDRESS AMGetIndirectIndexedAddress(Clock &clk, Memory &mem, bool PenaltyCycle);

    ADDRESS AMGetIndirectAddress(Clock &clk, Memory &mem);

    ADDRESS AMGetRelativeAddress(Clock &clk, Memory &mem, BYTE Offset);

    OP_RESULT Add(BYTE OpA, BYTE OpB);

    OP_RESULT BCDAdd(BYTE OpA, BYTE OpB);

    OP_RESULT Subtract(BYTE OpA, BYTE OpB);

    OP_RESULT BCDSubtract(BYTE OpA, BYTE OpB);

    OP_RESULT BitTest(BYTE OpA, BYTE OpB);

    OP_RESULT Compare(BYTE OpA, BYTE OpB);

    OP_RESULT ExclusiveOR(BYTE OpA, BYTE OpB);

    OP_RESULT RightShift(BYTE Operand);

};

#endif