#ifndef DEFS_H
#define DEFS_H

#ifndef STDINT_H
    #include <stdint.h>
#endif

using BYTE = uint8_t;
using ADDRESS = uint16_t;

#endif